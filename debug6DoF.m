function debug6DoF(imgTemplate, imgWebCam, img_curr, img_prev, dep_prev, K, num_levels, IntrinsicParams, Pose)
clc;
mex cpp6DoF.cpp
mex raster.cpp

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             MatLab
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[pose_rel, score, matlabPointclouds, matlabWarpedPointclouds, matlabWaperImgCoordinates, ...
    matlabGx, matlabGy, matlabInterpGx, matlabInterpGy, matlabGt, matlabComp_jacobian, ...
    matlabIncrementNumerador, matlabIncrementNumeradorInv, matlabIncrementDenominador]...
    = estimateVisualOdometry(img_curr, img_prev, dep_prev, K, num_levels, IntrinsicParams, Pose);

% pose_rel(1:3,1:3) = inv(pose_rel(1:3,1:3)); 

% eul = [0 5 0];
% rotm = eul2rotm(deg2rad(eul), 'XYZ');
% pose_rel = eye(4);
% pose_rel(1:3, 1:3) =  rotm;
% pose_rel(1:3, 4) = [0.01, -0.01, 0.01];
% pose_rel(1:3, 1:3) = inv(pose_rel(1:3, 1:3));
% pose_rel(1:3, 4) = -pose_rel(1:3, 4);

% Y negativo, es horizontal direccion derecha.
% Y positivo, es horizontal direccion izquierda.
% X negativo, es vertical direccion arriba.
% X positivo, es vertical direccion abajo.
% Z negativo, es mas chico.
% Z positivo, es mas grande.

% ROT Y parece ser X
% ROT X parece ser Y

[warped, valid_mask] = warpImage(double(imgTemplate), dep_prev, pose_rel, K, IntrinsicParams, Pose);


figure()
imageMatch = ShowMatch(uint8(imgWebCam), uint8(imgTemplate), [0, 0]);
imshow(flip(imageMatch))
figure()
imageMatch6DoF =( ShowMatch(imageMatch, uint8(warped), [0, 0]) );
imshow(flip(imageMatch6DoF))

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             CPP
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


% [cppPointclouds, cppWarpedPointclouds, cppWaperImgCoordinates, cppGx, cppGy, cppInterpGx, cppInterpGy, ...
%     cppGt, cppComp_jacobian, cppIncrementNumerador, cppIncrementNumeradorInv, cppIncrementDenominador] ...
%     = cpp6DoF(img_curr, img_prev, dep_prev, K, num_levels, height, width);

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             RASTER CPP
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
obj = readObj('impart.obj');

[height, width] = size(img_curr);

[frameBuffer2] = ...
    raster(obj.v, obj.vn, obj.f.v, length(obj.v),length(obj.f.v), height, width, IntrinsicParams, (pose_rel) * Pose);

figure()
imageMatch6DoF =( ShowMatch(imageMatch, (uint8(frameBuffer2')), [0, 0]) );
imshow(flip(imageMatch6DoF))


return;

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             Validation
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

validationPointclouds = [max(max(abs(matlabPointclouds-cppPointclouds))),...
    mean(mean(abs(matlabPointclouds-cppPointclouds)))];

validationWarpedPointclouds = [max(max(abs(matlabWarpedPointclouds-cppWarpedPointclouds))),...
    mean(mean(abs(matlabWarpedPointclouds-cppWarpedPointclouds)))];

validationWaperImgCoordinates = [max(max(abs(matlabWaperImgCoordinates-cppWaperImgCoordinates))),...
    mean(mean(abs(matlabWaperImgCoordinates-cppWaperImgCoordinates)))];

validationGx = [max(max(abs(matlabGx-cppGx))),...
    mean(mean(abs(matlabGx-cppGx)))];

validationGy = [max(max(abs(matlabGy-cppGy))),...
    mean(mean(abs(matlabGy-cppGy)))];

validationInterpGx = [max(max(abs(matlabInterpGx-cppInterpGx))),...
    mean(mean(abs(matlabInterpGx-cppInterpGx)))];

validationInterpGy = [max(max(abs(matlabInterpGy-cppInterpGy))),...
    mean(mean(abs(matlabInterpGy-cppInterpGy)))];

validationGt = [max(max(abs(matlabGt-cppGt))),...
    mean(mean(abs(matlabGt-cppGt)))];

validationComp_jacobian = [max(max(abs(matlabComp_jacobian-cppComp_jacobian))),...
    mean(mean(abs(matlabComp_jacobian-cppComp_jacobian)))];

validationIncrementNumerador = [max(max(abs(matlabIncrementNumerador-cppIncrementNumerador))),...
    mean(mean(abs(matlabIncrementNumerador-cppIncrementNumerador)))];

validationIncrementDenominador = [max(max(abs(matlabIncrementDenominador-cppIncrementDenominador))),...
    mean(mean(abs(matlabIncrementDenominador-cppIncrementDenominador)))];

