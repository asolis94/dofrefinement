#include "mex.hpp"
#include "mexAdapter.hpp"
#include "pch.h"

using namespace matlab::data;
using matlab::mex::ArgumentList;
using namespace std;

class MexFunction : public matlab::mex::Function
{
public:

	template <typename T>
	TypedArray<T> TransformMatrixToMATLAB(
		CONST IN Matrix<T>& inputMatrix)
	{
		ArrayFactory factory;
		ArrayDimensions dimen = { (size_t)inputMatrix.resolution.width, (size_t)inputMatrix.resolution.height };
		TypedArray<T> outTypedArray = factory.createArray<T>(dimen);

		size_t i, j;
		for (i = 0; i < (size_t)inputMatrix.resolution.width; i++)
		{
			for (j = 0; j < (size_t)inputMatrix.resolution.height; j++)
			{
				outTypedArray[i][j] = inputMatrix.data[i][j];
			}
		}

		return outTypedArray;
	}

	template <typename T>
	Matrix<T> TransformMATLABToMatrix(
		CONST IN TypedArray<T>& inputMatrix,
		CONST IN Resolution& resolution)
	{
		Matrix<T> outputMatrix = Matrix<T>(resolution);

		size_t i, j;
		for (i = 0; i < (size_t)resolution.width; i++)
		{
			for (j = 0; j < (size_t)resolution.height; j++)
			{
				outputMatrix.data[i][j] = inputMatrix[i][j];
			}
		}

		return outputMatrix;
	}

	float Min3(
		const float& a,
		const float& b,
		const float& c)
	{
		return min(a, min(b, c));
	}

	float Max3(
		const float& a,
		const float& b,
		const float& c)
	{
		return max(a, max(b, c));
	}

	float Map(
		const float x,
		const float in_min,
		const float in_max,
		const float out_min,
		const float out_max)
	{
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	VOID ConvertToProjectedVertex(
		CONST IN Matrix44f& worldToCameraMatrix,
		CONST IN Matrix44f& projectionMatrix,
		CONST IN Vec3f& worldVertex,
		OUT Vec3f& cameraVertex,
		OUT Vec3f& projectedVertex)
	{
		worldToCameraMatrix.multPointMatrix(worldVertex, cameraVertex);
		projectionMatrix.multPointMatrix(cameraVertex, projectedVertex);
	}

	VOID ConvertToRaster(
		CONST IN Resolution& resolution,
		CONST IN Vec3f& cameraVertex,
		CONST IN Vec3f& projectedVertex,
		OUT Vec3f& vertexRaster)
	{
		vertexRaster.x = (projectedVertex.x + 1) * 0.5 * resolution.width;
		vertexRaster.y = (projectedVertex.y + 1) * 0.5 * resolution.height;
		vertexRaster.z = projectedVertex.z;
	}

	float EdgeFunction(
		CONST IN Vec3f& vertex_1,
		CONST IN Vec3f& vertex_2,
		CONST IN Vec3f& vertex_3)
	{
		return
			(vertex_3.x - vertex_1.x) *
			(vertex_2.y - vertex_1.y) -
			(vertex_3.y - vertex_1.y) *
			(vertex_2.x - vertex_1.x);
	}

	VOID RasterScene(
		CONST IN Mesh& mesh,
		CONST IN Matrix44f& projectionMatrix,
		CONST IN Matrix44f& worldToCameraMatrix,
		OUT Image<float>& frameBuffer,
		OUT Image<float>& depthBuffer)
	{
		float MaxZ = abs(worldToCameraMatrix.GetTranslation().z * 2.0f);

		float C = projectionMatrix.x[2][2];
		float D = projectionMatrix.x[2][3];
		float n = D / (C - 1.0);
		float f = D / (C + 1.0);

		if (isnan(f))
			f = 1000.0f;

		depthBuffer = Image<float>(frameBuffer.resolution, f);

		Vec3f lightAngle = worldToCameraMatrix.GetRow(2).toVec3();

		for (int i = 0; i < mesh.triangles.size(); ++i)
		{
			const Vec3f& v0World = mesh.vertex[mesh.triangles[i].x].pos;
			const Vec3f& v1World = mesh.vertex[mesh.triangles[i].y].pos;
			const Vec3f& v2World = mesh.vertex[mesh.triangles[i].z].pos;

			Vec3f v0Camera, v1Camera, v2Camera,
				v0Projected, v1Projected, v2Projected;

			ConvertToProjectedVertex(worldToCameraMatrix, projectionMatrix, v0World, v0Camera, v0Projected);
			ConvertToProjectedVertex(worldToCameraMatrix, projectionMatrix, v1World, v1Camera, v1Projected);
			ConvertToProjectedVertex(worldToCameraMatrix, projectionMatrix, v2World, v2Camera, v2Projected);

			Vec3f v0Raster, v1Raster, v2Raster;

			ConvertToRaster(frameBuffer.resolution, v0Camera, v0Projected, v0Raster);
			ConvertToRaster(frameBuffer.resolution, v1Camera, v1Projected, v1Raster);
			ConvertToRaster(frameBuffer.resolution, v2Camera, v2Projected, v2Raster);

			if (v0Projected.x < -1 || v0Projected.x > 1 || v0Projected.y < -1 || v0Projected.y > 1) continue;

			if (v1Projected.x < -1 || v1Projected.x > 1 || v1Projected.y < -1 || v1Projected.y > 1) continue;

			if (v2Projected.x < -1 || v2Projected.x > 1 || v2Projected.y < -1 || v2Projected.y > 1) continue;

			if (v0Raster.z < n || v1Raster.z < n || v2Raster.z < n)
				continue;

			v0Raster.z = 1 / v0Raster.z;
			v1Raster.z = 1 / v1Raster.z;
			v2Raster.z = 1 / v2Raster.z;

			Vec3f n0, n1, n2;
			n0 = mesh.vertex[mesh.triangles[i].x].normal * v0Raster.z;
			n1 = mesh.vertex[mesh.triangles[i].y].normal * v1Raster.z;
			n2 = mesh.vertex[mesh.triangles[i].z].normal * v2Raster.z;

			float xmin = Min3(v0Raster.x, v1Raster.x, v2Raster.x);
			float ymin = Min3(v0Raster.y, v1Raster.y, v2Raster.y);
			float xmax = Max3(v0Raster.x, v1Raster.x, v2Raster.x);
			float ymax = Max3(v0Raster.y, v1Raster.y, v2Raster.y);

			if (xmin > frameBuffer.resolution.width - 1 || xmax < 0 || ymin > frameBuffer.resolution.height - 1 || ymax < 0)
				continue;

			int x0 = max(int(0), (int)(floor(xmin)));
			int x1 = min(int(frameBuffer.resolution.width) - 1, (int)(floor(xmax)));
			int y0 = max(int(0), (int)(floor(ymin)));
			int y1 = min(int(frameBuffer.resolution.height) - 1, (int)(floor(ymax)));

			float area = EdgeFunction(v0Raster, v1Raster, v2Raster);

			if (area <= 0)
				continue;

			int x, y;
			for (x = x0; x <= x1; ++x)
			{
				for (y = y0; y <= y1; ++y)
				{
					Vec3f pixelSample(x + 0.5, y + 0.5, 0);
					float w0 = EdgeFunction(v1Raster, v2Raster, pixelSample);
					float w1 = EdgeFunction(v2Raster, v0Raster, pixelSample);
					float w2 = EdgeFunction(v0Raster, v1Raster, pixelSample);

					if (w0 >= 0 && w1 >= 0 && w2 >= 0)
					{
						w0 /= area;
						w1 /= area;
						w2 /= area;
						float oneOverZ = v0Raster.z * w0 + v1Raster.z * w1 + v2Raster.z * w2;
						float z = 1 / oneOverZ;

						if (z < depthBuffer.data[x][y])
						{
							depthBuffer.data[x][y] = z;

							Vec3f nor = (n0 * w0) + (n1 * w1) + (n2 * w2);
							nor *= z;

							Vec3f viewDirection = nor;
							viewDirection.normalize();

							// Range [1, -1] siendo 1 lo mas iluminado y -1 negro
							float nDotView = max(-0.25f, lightAngle.dotProduct(viewDirection));
							nDotView = min(1.0f, nDotView);
							nDotView = Map(nDotView, -0.25f, 1.0f, 0.1f, 1.0f);

							// Depth Light
							float depthLight = Map(z, 0.0f, MaxZ, 1.0f, 0.1f);
							frameBuffer.data[x][y] = 255;
						}
					}
				}
			}
		}
	}

	VOID operator()(ArgumentList outputs, ArgumentList inputs)
	{
		TypedArray<double> vertex = std::move(inputs[0]);
		TypedArray<double> normals = std::move(inputs[1]);
		TypedArray<double> triangles = std::move(inputs[2]);

		int vertexCount = inputs[3][0];
		int triangleCount = inputs[4][0];

		size_t height = inputs[5][0];
		size_t width = inputs[6][0];

		TypedArray<double> intrinsicParams = std::move(inputs[7]);
		TypedArray<double> pose = std::move(inputs[8]);

		Matrix<double> sourceVertices = TransformMATLABToMatrix(vertex, Resolution(vertexCount, 3));
		Matrix<double> sourceNormals = TransformMATLABToMatrix(normals, Resolution(vertexCount, 3));
		Matrix<double> sourceTriangles = TransformMATLABToMatrix(triangles, Resolution(triangleCount, 3));

		Mesh mesh = Mesh(vertexCount, triangleCount);

		int i, j;
		for (i = 0; i < mesh.vertex.size(); ++i)
		{
			Mesh::MeshVertex& v = mesh.vertex[i];
			v.pos = Vec3f(sourceVertices[i][0], sourceVertices[i][1], sourceVertices[i][2]);
			v.normal = Vec3f(sourceNormals[i][0], sourceNormals[i][1], sourceNormals[i][2]);

			if (i < mesh.triangles.size())
			{
				Vec3i& t = mesh.triangles[i];
				t.x = sourceTriangles[i][0] - 1;
				t.y = sourceTriangles[i][1] - 1;
				t.z = sourceTriangles[i][2] - 1;
			}
		}

		Matrix<double> IntrinsicParameters = TransformMATLABToMatrix(intrinsicParams, Resolution(4, 4));
		Matrix<double> Pose = TransformMATLABToMatrix(pose, Resolution(4, 4));
		Resolution resolution = Resolution(width, height);

		Matrix44f projectionMatrix, viewMatrix;
		for (i = 0; i < IntrinsicParameters.rows; ++i)
		{
			for (j = 0; j < IntrinsicParameters.columns; ++j)
			{
				projectionMatrix.x[i][j] = IntrinsicParameters[i][j];
				viewMatrix.x[i][j] = Pose[i][j];
			}
		}

		Image<float> frameBuffer = Image<float>(resolution);
		Image<float> depthBuffer;

		RasterScene(mesh, projectionMatrix, viewMatrix, frameBuffer, depthBuffer);

		Vec3f Worldvertex(0.181138000000000, 0.113331000000000, 0.0255640000000000);
		Vec3f Cameravertex;
		Vec3f Projectedvertex;
		Vec3f pixel;

		cout << "Worldvertex: " << Worldvertex << endl;

		viewMatrix.multPointMatrix(Worldvertex, Cameravertex);

		cout << "CameraVertex: " << Cameravertex << endl;

		projectionMatrix.multPointMatrix(Cameravertex, Projectedvertex);

		cout << "Projectedvertex: " << Projectedvertex << endl;

		pixel.x = (Projectedvertex.x + 1) * 0.5 * resolution.width;
		pixel.y = (Projectedvertex.y + 1) * 0.5 * resolution.height;
		pixel.z = Projectedvertex.z;

		cout << "pixel: " << pixel << endl;

		Projectedvertex.x = (pixel.x / resolution.width / 0.5) - 1;
		Projectedvertex.y = (pixel.y / resolution.height / 0.5) - 1;
		Projectedvertex.z = pixel.z;

		cout << "Recovered Projectedvertex: " << Projectedvertex << endl;

		projectionMatrix.inverse().multPointMatrix(Projectedvertex, Cameravertex);

		cout << "Recovered Cameravertex: " << Cameravertex << endl;

		viewMatrix.inverse().multPointMatrix(Cameravertex, Worldvertex);

		cout << "Recovered Worldvertex: " << Worldvertex << endl;

		outputs[0] = TransformMatrixToMATLAB(frameBuffer);
	}
};