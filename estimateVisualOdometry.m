function [pose_rel, score, debugPointclouds, debugWarpedPointClouds, debugWarpedImgCoordinates,...
    matlabGx, matlabGy, matlabInterpGx, matlabInterpGy, matlabGt, matlabComp_jacobian,...
    matlabIncrementNumerador, matlabIncrementNumeradorInv, matlabIncrementDenominador] = ...
    estimateVisualOdometry(img_curr, img_prev, dep_prev, K, num_levels, IntrinsicParams, Pose)

% construct image pyramids
img_curr_pyr = constructPyramid(img_curr, num_levels); % DONE
img_prev_pyr = constructPyramid(img_prev, num_levels);  % DONE

% construct depth pyramid for the previous frame
dep_prev_pyr = constructPyramid(dep_prev, num_levels);  % DONE

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% estimate the relative pose from coarse to fine scales

% initialize the relative pose and its increment
pose_rel = eye(4);
increment = zeros(6, 1);

% convert the vectorize motion increment to update the relative pose
increment = twistexp(increment);   % DONE
pose_rel = increment * pose_rel;     % DONE

% modify the camera parameters to fit each pyramid
K_pyr = K;
K_pyr(1:2, :) = K_pyr(1:2, :) / (2^(num_levels-1));   % DONE

for n = num_levels:-1:1
    
    % image size
    [height, width] = size(dep_prev_pyr{n});
    
    % get valid point clouds in the previous frame
    [pointclouds, valid_mask] = reprojectDepthImage(dep_prev_pyr{n}, K_pyr, IntrinsicParams, Pose); % DONE
    
    % warp pointclouds and prune invalid points
    warped_pointclouds = warpPointCloud(pointclouds,inv( pose_rel)); % DONE
    
%     figure(1)
%     hold on
%     pcshow(warped_pointclouds, [rand,rand,rand])
    
    [warped_img_coordinates, valid_points] = projectPointCloud(warped_pointclouds, IntrinsicParams, K_pyr, height, width, Pose);  % DONE
    
    warped_pointclouds = warped_pointclouds(valid_points, :);  % DONE
    pointclouds = pointclouds(valid_points, :);  % DONE
    
    %----------------------------------------------------------------------
    debugPointclouds = pointclouds;                                         %Salida para debugueo ++++++++++++++
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    debugWarpedPointClouds = warped_pointclouds;                            %Salida para debugueo +++++++++
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    debugWarpedImgCoordinates = warped_img_coordinates;                     %Salida para debugueo ++++++++
    %----------------------------------------------------------------------
    
     %----------------------------------------------------------------------
%     warpedImaaage = zeros(size(dep_prev_pyr{n}));
% for i = 1:length(warped_img_coordinates)
%      warpedImaaage(uint32(warped_img_coordinates(i,2)), uint32(warped_img_coordinates(i,1))) = 255;
% end
%     figure()
%     imshow(warpedImaaage)
    
    %spatial gradient in the current frame
    [Gx_curr, Gy_curr] = imgradientxy(img_curr_pyr{n}, 'CentralDifference'); % DONE
    
    %----------------------------------------------------------------------
    matlabGx = Gx_curr;                                                     %Salida para debugueo ++++++++
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    matlabGy = Gy_curr;                                                     %Salida para debugueo ++++++++
    %----------------------------------------------------------------------
    
    Gx_curr = interp2(Gx_curr, warped_img_coordinates(:, 1), warped_img_coordinates(:, 2), 'linear', 0); % DONE
    Gy_curr = interp2(Gy_curr, warped_img_coordinates(:, 1), warped_img_coordinates(:, 2), 'linear', 0); % DONE
    Gs_curr = cat(2, Gx_curr, Gy_curr); % DONE
    
    %----------------------------------------------------------------------
    matlabInterpGx = Gx_curr;                                                     %Salida para debugueo ++++++++
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    matlabInterpGy = Gy_curr;                                                     %Salida para debugueo ++++++++
    %----------------------------------------------------------------------
    
    % temporal visual difference
    Gt_prev = img_prev_pyr{n}; % Template image format % DONE
    Gt_prev = Gt_prev(valid_mask); %  % DONE
    Gt_prev = Gt_prev(valid_points); % DONE
    Gt_curr = img_curr_pyr{n}; % WebCam % DONE
    Gt_curr = interp2(Gt_curr, warped_img_coordinates(:, 1), warped_img_coordinates(:, 2), 'linear', 0); % DONE
    Gt = Gt_curr - Gt_prev; % DONE
    
    
    %----------------------------------------------------------------------
    matlabGt = Gt;                                                     %Salida para debugueo ++++++++
    %----------------------------------------------------------------------
    
    
    % calculate the warping Jacobian
    warp_jacobian = calculateWarpingJacobian(warped_pointclouds, pointclouds, pose_rel, K_pyr);
    
    % calculate the compositive jacobian
    comp_jacobian = squeeze(sum(bsxfun(@times, Gs_curr, warp_jacobian), 2));
    
    %----------------------------------------------------------------------
    matlabComp_jacobian = comp_jacobian;                                                     %Salida para debugueo ++++++++
    %----------------------------------------------------------------------
    
    % calculate the increment motion
    matlabIncrementNumerador = -(comp_jacobian'*comp_jacobian);
    matlabIncrementNumeradorInv = inv(-(comp_jacobian'*comp_jacobian));
    matlabIncrementDenominador = (comp_jacobian'*Gt);
    %increment = -(comp_jacobian'*comp_jacobian)\(comp_jacobian'*Gt);
    increment = matlabIncrementNumeradorInv  * matlabIncrementDenominador;
    
    % get the current relative pose
    increment = twistexp(increment);
    pose_rel = increment * pose_rel;
    
    % increse the focal length
    K_pyr(1:2, :) = K_pyr(1:2, :) * 2; % DONE
end



% get the final score
% [warped_image, valid_mask] = warpImage(img_curr, dep_prev, pose_rel, K);
% score = mean((warped_image(valid_mask) - img_prev(valid_mask)).^2);
score = 0;
end
