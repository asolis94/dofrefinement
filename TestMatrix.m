clc;
close all;

imgWebCam = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\imgWebCam.txt"))' );
imgTemplate = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\imgTemplate.txt"))' );
img_curr = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\img_curr.txt"))' );
img_prev = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\img_prev.txt"))' );
dep_prev = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\dep_prev.txt"))' );

eul = [1 5 3];
rotm = eul2rotm(deg2rad(eul), 'XYZ');
pose_rel = eye(4);
pose_rel(1:3, 1:3) =  rotm;
pose_rel(1:3, 4) = [0.01, -0.01, 0.01];


[pointclouds, valid_mask] = reprojectDepthImage(dep_prev, K, IntrinsicParams);
warped_pointclouds = warpPointCloud(pointclouds, pose_rel);

figure(1)
hold on;
pcshow(pointclouds, 'red');
pcshow(warped_pointclouds, 'green');
title('Red Original PC - Green Warped PC');
xlabel('X');
ylabel('Y');
zlabel('Z');

[warped_img_coords, warped_valid_mask] = projectPointCloud(warped_pointclouds, IntrinsicParams, K, height, width);

figure()
imshow(flip(dep_prev))
warpedImaaage = zeros(size(dep_prev));
for i = 1:length(warped_img_coords)
    warpedImaaage(uint32(warped_img_coords(i,2)), uint32(warped_img_coords(i,1))) = 255;
end
figure()
imshow(flip(warpedImaaage))

figure()
imshow(flip(dep_prev) + flip(warpedImaaage))


obj = readObj('impart.obj');

[frameBuffer1] = ...
    raster(obj.v, obj.vn, obj.f.v, length(obj.v),length(obj.f.v), height, width, IntrinsicParams, Pose);

figure()
imshow(flip(uint8(frameBuffer1')));

[frameBuffer2] = ...
    raster(obj.v, obj.vn, obj.f.v, length(obj.v),length(obj.f.v), height, width, IntrinsicParams, pose_rel * Pose);

figure()
imshow(flip(uint8(frameBuffer2')));

figure()
imshow(flip(uint8(frameBuffer1')) + flip(uint8(frameBuffer2')))