clear all; close all; clc;

mex raster.cpp
mex cpp6DoF.cpp

addpath(genpath('./Dataset 6DoF'));
exp_folder = 'Impart';
K = dlmread(fullfile(exp_folder, 'mbtParams.txt'));
num_levels = 5;
imgWebCam = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\imgWebCam.txt"))' );
imgTemplate = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\imgTemplate.txt"))' );
img_curr = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\img_curr.txt"))' );
img_prev = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\img_prev.txt"))' );
dep_prev = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\dep_prev.txt"))' );

IntrinsicParams = dlmread(fullfile("D:\Unity\mbt-integration-platform\k.txt"));
Pose = dlmread(fullfile("D:\Unity\mbt-integration-platform\pose.txt"));

[height, width] = size(img_curr);
% figure(1)
% imageMatch =flip( ShowMatch(uint8(imgWebCam'), uint8(imgTemplate'), [0, 0]) );
% imshow(imageMatch)

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             MatLab
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[pose_rel, score, matlabPointclouds, matlabWarpedPointclouds, matlabWaperImgCoordinates, ...
    matlabGx, matlabGy, matlabInterpGx, matlabInterpGy, matlabGt, matlabComp_jacobian, ...
    matlabIncrementNumerador, matlabIncrementNumeradorInv, matlabIncrementDenominador]...
    = estimateVisualOdometry(img_curr, img_prev, dep_prev, K, num_levels, IntrinsicParams);

poseee_rel = pose_rel;

% eul = [0 5 0];
% rotm = eul2rotm(deg2rad(eul), 'XYZ');
% pose_rel = eye(4);
% pose_rel(1:3, 1:3) =  rotm;
% pose_rel(1:3, 4) = [0.01, -0.01, 0.01];

pose_rel
%eul = rotm2eul(poseModel(1:3,1:3))

% Y negativo, es horizontal direccion derecha.
% Y positivo, es horizontal direccion izquierda.
% X negativo, es vertical direccion arriba.
% X positivo, es vertical direccion abajo.
% Z negativo, es mas chico.
% Z positivo, es mas grande.

% ROT Y parece ser X
% ROT X parece ser Y

[warped, valid_mask] = warpImage(double(imgTemplate), dep_prev, pose_rel, K, IntrinsicParams);


figure()
imageMatch = ShowMatch(uint8(imgWebCam), uint8(imgTemplate), [0, 0]);
imshow(flip(imageMatch))
figure()
imageMatch6DoF =( ShowMatch(imageMatch, uint8(warped), [0, 0]) );
imshow(flip(imageMatch6DoF))

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             CPP
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


% [cppPointclouds, cppWarpedPointclouds, cppWaperImgCoordinates, cppGx, cppGy, cppInterpGx, cppInterpGy, ...
%     cppGt, cppComp_jacobian, cppIncrementNumerador, cppIncrementNumeradorInv, cppIncrementDenominador] ...
%     = cpp6DoF(img_curr, img_prev, dep_prev, K, num_levels, height, width);

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             RASTER CPP
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
obj = readObj('impart.obj');

[frameBuffer1] = ...
    raster(obj.v, obj.vn, obj.f.v, length(obj.v),length(obj.f.v), height, width, IntrinsicParams, Pose);

figure()
imshow(flip(uint8(frameBuffer1')));

[frameBuffer2] = ...
    raster(obj.v, obj.vn, obj.f.v, length(obj.v),length(obj.f.v), height, width, IntrinsicParams, pose_rel * Pose);

figure()
imshow(flip(uint8(frameBuffer2')));

figure()
frameBufferDif1 = zeros(height, width, 3);
frameBufferDif1(:,:,1) = flip(uint8(frameBuffer1'));
frameBufferDif2 = zeros(height, width, 3);
frameBufferDif2(:,:,2) = flip(uint8(frameBuffer2'));

imshow(frameBufferDif1 + frameBufferDif2)


%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%                             DEBUG
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%warpImage2(double(imgTemplate), dep_prev, poseModel, Pose, K, IntrinsicParams);