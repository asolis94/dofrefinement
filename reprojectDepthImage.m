function [pointclouds, valid_mask] = reprojectDepthImage(depth, K, IntrinsicParams, Pose)
% Reproject the depth image to 3D point clouds
%
% INPUT:
%   depth: a grayscale image
%   K: intrinsic parameters of the camera
%
% OUTPUT:
%   pointclouds: a 3-channel point list
%   valid_mask: a binary mask

height = size(depth, 1);
width = size(depth, 2);

% TODO: This has to be a valid mask avoiding the space that it is INF.
valid_mask = true(size(depth));
valid_mask = depth < 1000 ;

normalized_coords = constructNormalizedCoordinate(height, width, K);

pointclouds = cat(3, bsxfun(@times, normalized_coords, depth), depth);

% convert pointclouds to a list
pointclouds = reshape(pointclouds, [], 3);
pointclouds = pointclouds(valid_mask, :);


% Backproject DepthBuffer in to 3D WorldCoordinates
pointclouds = zeros(size(pointclouds));
index = 1;
for i = 1:width
    for j = 1:height
        if(depth(j, i) < 1000 )
            
            px = [j, i, depth(j,i)];
            
            rpj = [(px(2) / height / 0.5) - 1, (px(1) / width / 0.5) - 1,px(3)];
            rcv = multPointMatrix(inv(IntrinsicParams), rpj);
            
            %rwv = multPointMatrix(inv(Pose), rcv);

            pointclouds(index,:) = rcv;
            index = index + 1;
        end
    end
end

end