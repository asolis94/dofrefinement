function [P] = findPerpendicularPoints(y1,x1,y2,x2,N)

P = zeros((2*N)+1,2);
p1 = mean([x1,x2]);
p2 = mean([y1,y2]);

if (y2-y1) ~= 0 && (x2-x1) ~= 0  
 
    m1 = (y2-y1)/(x2-x1);
    m2 = -1/m1;  

    c = 1; 
    for i=p1-N:1:p1+N
        j=m2*i-(m2*p1)+p2;
        P(c,:) = [j,i];
        c = c + 1; 
    end
 
elseif (x2-x1) == 0

    c = 1; 
    for i=p1-N:1:p1+N
        j=p2; 
        P(c,:) = [j,i];
        c = c + 1; 
    end
    
elseif (y2-y1) == 0 
        
    c = 1; 
    for j=p2-N:1:p2+N
        i=p1;
        P(c,:) = [j,i];
        c = c + 1; 
    end 
        
end


