function warpImage2(img_curr, dep_prev, pose_rel, Pose, K, IntrinsicParams)
% warp the current image to the previous image coordinate
%
% INPUT:
%   img_curr: current grayscale image
%   dep_prev: previous depth image
%   pose_rel: relative pose between the current and previous frame
%   K: intrinsic camera parameters
%
% OUTPUT:
%   warped_image: warped image by the rigid transformation
%   valid_mask: valid pixels in the warped image

[height, width] = size(img_curr);


[pointclouds, valid_mask] = reprojectDepthImage(dep_prev, IntrinsicParams);
warped_pointclouds = warpPointCloud(pointclouds, pose_rel);

figure(5);
hold on;
xlabel("X")
ylabel("Y")
zlabel("Z");
pcshow(pointclouds);

figure(7)
dep_prev = flip(dep_prev');
imshow(uint8(dep_prev));
[height, width] = size(dep_prev);
% Backproject DepthBuffer in to 3D WorldCoordinates
cameraVertex = [];
for i = 1:width
    for j = 1:height
        if(dep_prev(j, i) < 1000)
          
            px = [j, i, dep_prev(j,i)];

            rpj = [(px(2) / height / 0.5) - 1, (px(1) / width / 0.5) - 1,px(3)];
            rcv = multPointMatrix(inv(IntrinsicParams), rpj);
            
            cameraVertex = [cameraVertex; rcv]; 
           
        end
    end
end
imageDepthBuffer = size(size(img_curr));


for i = 1:length(cameraVertex)
    
    pj = multPointMatrix(IntrinsicParams, cameraVertex(i,:));
    px = [(pj(2) + 1) * 0.5 * width, (pj(1) + 1) * 0.5 * height, pj(3)];
    imageDepthBuffer(uint32(px(1)),uint32(px(2))) = 255;
    
end
%[warped_img_coords, warped_valid_mask] = projectPointCloud(warped_pointclouds, K, height, width);
figure(6);
hold on;
xlabel("X")
ylabel("Y")
zlabel("Z");
pcshow(cameraVertex);
Origin = Pose(1:3,4)';
pcshow([Origin(1), Origin(2), Origin(3)], 'red', 'MarkerSize', 24);


figure(8)
imshow(uint8(imageDepthBuffer));
end