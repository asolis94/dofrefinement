function anchorPoints = findAnchors(segmentPoints,N)

    c = 1; 
    anchorPoints = [];
    for k=1:N:size(segmentPoints,1)-N
        
        x1 = segmentPoints(k,1);
        y1 = segmentPoints(k,2);
        x2 = segmentPoints(k+N,1);
        y2 = segmentPoints(k+N,2);
            
        d = sqrt(abs(x1-x2)^2+abs(y1-y2)^2);
        D = sqrt(N^2+N^2);
        
        if d<=D && segmentPoints(k,4) == segmentPoints(k+N,4)
            anchorPoints(c,:) = [y1,x1,y2,x2];
            c = c + 1; 
        end
        
    end
    
