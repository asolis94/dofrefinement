function ROI = getROI(imageTemplate, translationConstraint)
   
    ROI = zeros(size(imageTemplate));
    for i=1:1:size(imageTemplate,1)
        for j=1:1:size(imageTemplate,2)
            
            if imageTemplate(i,j) >= 10
                ROI(i-translationConstraint:i+translationConstraint,j-translationConstraint:j+translationConstraint) = 1;               
            end
           
        end
    end
   
end