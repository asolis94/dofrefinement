clear all; close all; clc


load cppPointclouds.mat
load cppWarpedPointclouds.mat
load pose_rel.mat
load matlabWarpedPointclouds.mat

r = cppPointclouds(1,1)*pose_rel(1:3,1)
