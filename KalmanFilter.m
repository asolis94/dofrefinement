function  [x1Kalman, iKalman] = KalmanFilter(x0Kalman, zKalman, nKalman)

    iKalman = nKalman + 1; 
    x1Kalman =  x0Kalman + 1/iKalman*(zKalman- x0Kalman);
    
    if iKalman>15
        iKalman = 0;
        x1Kalman = zKalman; 
    end
    
end