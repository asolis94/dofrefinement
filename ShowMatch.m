function  webCamRaw = ShowMatch(webCamRaw, imageTemplate, TranslationVector)
if(size(webCamRaw,3) == 1)
    webCamRaw = uint8(cat(3, webCamRaw, webCamRaw, webCamRaw));
end

color = rand(3,1);

for x=1:1:size(imageTemplate,1)
    for y=1:1:size(imageTemplate,2)
        if imageTemplate(x,y) ~=0
            
            xt = x - TranslationVector(1);
            yt = y - TranslationVector(2);
            
            if(xt > 0 && yt > 0 && xt < size(imageTemplate,1) && yt < size(imageTemplate,2))
                webCamRaw(xt,yt,1) = imageTemplate(x,y)*color(1);
                webCamRaw(xt,yt,2) = imageTemplate(x,y)*color(2);
                webCamRaw(xt,yt,3) = imageTemplate(x,y)*color(3);
            end
        end
    end
end

end

