function img_pyr = constructPyramid(img, num_levels)
    img_pyr = cell(num_levels, 1);
    img_pyr{1} = img;
    for i = 2:num_levels
        img = img(1:2:end, 1:2:end);  
        img_pyr{i} = img;
    end
end