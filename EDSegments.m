function [segments, edges] = EDSegments(rawImage, ROI)

    gaussianKernel = fspecial('gaussian', 5, 2);

    %RGB to Gray 
    grayImage = rawImage;

    %Gaussian smoothing 
    gaussianImage = conv2(grayImage, gaussianKernel, 'same');

    %Gradient operations
    [gradientsImage, directionsImage] = gradientOperations(gaussianImage);
    
    %Anchors extraction
    [edgesImage, anchorPoints] = anchorsExtaction(gradientsImage, directionsImage, ROI);
    
    %Join anchors
    [edges, segments] = joinAnchors(gradientsImage, directionsImage, edgesImage, anchorPoints);
    
    %Gradient operations -----------------------------------------------
    %--------------------------------------------------------------------------

    function [gradientsImage, directionsImage] = gradientOperations(gaussianImage)

        gradientThreshold = 36;
        gxImage = zeros(size(gaussianImage));
        gyImage = zeros(size(gaussianImage));
        gradientsImage = zeros(size(gaussianImage));
        directionsImage = zeros(size(gaussianImage));

        for x = 2:1:size(gaussianImage, 1)-1
            for y = 2:1:size(gaussianImage, 2)-1

                gxImage(x,y) = abs (-gaussianImage(x-1,y-1) + gaussianImage(x-1,y+1) + ...
                                    -2*gaussianImage(x,y-1) + 2*gaussianImage(x,y+1) + ...
                                    -gaussianImage(x+1,y-1) + gaussianImage(x+1,y+1));

                gyImage(x,y) = abs (-gaussianImage(x-1,y-1) + gaussianImage(x+1,y-1) + ...
                                    -2*gaussianImage(x-1,y) + 2*gaussianImage(x+1,y) + ...
                                    -gaussianImage(x-1, y+1) + gaussianImage(x+1, y+1));

                gradientsImage(x,y) = gxImage(x,y) + gyImage(x,y);

                if gradientsImage(x,y) >= gradientThreshold
                    if (gxImage(x,y) >= gyImage(x,y))
                        directionsImage(x,y) = 1;
                    else
                        directionsImage(x,y) = 2;
                    end
                end

            end
        end

    end    

    %Anchors extraction ------------------------------------------------ 
    %--------------------------------------------------------------------------

    function [edgesImage, anchorPoints] = anchorsExtaction(gradientsImage, directionsImage, ROI)

        anchorThreshold = 8;
        gradientThreshold = 36;
        idxAnchor = 1;
        anchorPoints = [];
        edgesImage = zeros(size(gradientsImage));

        for i = 2:1:size(gradientsImage,1)-1
            for j = 2:1:size(gradientsImage,2)-1

                if ROI(i,j) == 1      
                    if (gradientsImage(i, j) < gradientThreshold)
                        continue;
                    end

                    if (directionsImage(i, j) == 1)
                        diff1 = gradientsImage(i, j) - gradientsImage(i, j - 1);
                        diff2 = gradientsImage(i, j) - gradientsImage(i, j + 1);
                        if (diff1 >= anchorThreshold && diff2 >= anchorThreshold)
                            edgesImage(i, j) = 254;
                            anchorPoints(idxAnchor, :) = [i, j, gradientsImage(i, j)];
                            idxAnchor = idxAnchor + 1;
                        end
                    else
                        diff1 = gradientsImage(i, j) - gradientsImage(i - 1, j);
                        diff2 = gradientsImage(i, j) - gradientsImage(i + 1, j);
                        if (diff1 >= anchorThreshold && diff2 >= anchorThreshold)
                            edgesImage(i, j) = 254;
                            anchorPoints(idxAnchor, :) = [i, j, gradientsImage(i, j)];
                            idxAnchor = idxAnchor + 1;
                        end
                    end

                end          
            end
                
        end
    end

    %Join anchors --------------------------------------------------------
    %--------------------------------------------------------------------------

    function [EDImage, EDSegments] = joinAnchors(gradientsImage, directionsImage, edgeImge, anchorPoints)

        gradientThreshold = 36;
        minPathLen = 10;

        segmentIdx = 1;
        segmentCount = 1;
        EDSegments = [];

        for k = 1:size(anchorPoints, 1)

            aX = anchorPoints(k, 1);
            aY = anchorPoints(k, 2);

            if edgeImge(aX, aY) ~= 254
                continue;
            end

            pixels = []; 
            len = 0;
            duplicatePixelCount = 0;

            if directionsImage(aX, aY) == 2% Horizontal
                HorizontalRightNavigator(aX, aY);
                HorizontalLeftNavigator(aX, aY);
            else
                VerticalDownNavigator(aX, aY);
                VerticalUpNavigator(aX, aY);
            end

            if (len - duplicatePixelCount <= minPathLen)
                for p = 1:len
                    edgeImge(pixels(p, 1), pixels(p, 2)) = 0;
                end
            else
                
                for p = 1:1:len
                   
                    if p < len

                        EDSegments(segmentIdx, :) = [pixels(p, :), segmentCount];
                        segmentIdx = segmentIdx + 1;
                   
                        if abs(pixels(p,1) - pixels(p+1,1)) > 1 || abs(pixels(p,2) - pixels(p+1,2)) > 1
                            segmentCount = segmentCount + 1; 
                        end   

                    else
                        EDSegments(segmentIdx, :) = [pixels(p, :), segmentCount];                   
                    end         
                    
                end       
                
            end
            segmentCount = segmentCount + 1; 
        end

        EDImage = edgeImge;

        function HorizontalLeftNavigator(aX, aY)

            if (edgeImge(aX, aY) ~= 255)
                duplicatePixelCount = duplicatePixelCount + 1;
            end

            len = len + 1;
            pixels(len, :) = [aX, aY, 1];

            while (directionsImage(aX, aY) == 2)

                edgeImge(aX, aY) = 255;

                if (edgeImge(aX - 1, aY) == 254)
                    edgeImge(aX - 1, aY) = 0;
                end

                if (edgeImge(aX + 1, aY) == 254)
                    edgeImge(aX + 1, aY) = 0;
                end

                %//   A
                %//   B x
                %//   C

                % Follow next pixel in line
                aY = aY - 1; % B

                if (edgeImge(aX, aY) >= 254)% B
                elseif (edgeImge(aX - 1, aY) >= 254)% A
                    aX = aX - 1;
                elseif (edgeImge(aX + 1, aY) >= 254)% C
                    aX = aX + 1;
                else % A or C
                    A = gradientsImage(aX - 1, aY);
                    B = gradientsImage(aX, aY);
                    C = gradientsImage(aX + 1, aY);

                    if (A > B)

                        if (A > C)
                            aX = aX - 1; % A
                        else
                            aX = aX + 1; % C
                        end

                    elseif (C > B)
                        aX = aX + 1; % C
                    end

                end

                if edgeImge(aX, aY) == 255 || gradientsImage(aX, aY) < gradientThreshold
                    return;
                end

                len = len + 1;
                pixels(len, :) = [aX, aY, 1];
            end

            len = len - 1;

            VerticalDownNavigator(aX, aY);
            VerticalUpNavigator(aX, aY);
        end

        function HorizontalRightNavigator(aX, aY)

            if (edgeImge(aX, aY) ~= 255)
                duplicatePixelCount = duplicatePixelCount + 1;
            end

            len = len + 1;
            pixels(len, :) = [aX, aY, 1];

            while (directionsImage(aX, aY) == 2)

                edgeImge(aX, aY) = 255;

                if (edgeImge(aX + 1, aY) == 254)
                    edgeImge(aX + 1, aY) = 0;
                end

                if (edgeImge(aX - 1, aY) == 254)
                    edgeImge(aX - 1, aY) = 0;
                end

                %//     A
                %//   x B
                %//     C

                % Follow next pixel in line
                aY = aY + 1; % B

                if (edgeImge(aX, aY) >= 254)% B
                elseif (edgeImge(aX + 1, aY) >= 254)% C
                    aX = aX + 1;
                elseif (edgeImge(aX - 1, aY) >= 254)% A
                    aX = aX - 1;
                else % A or C
                    A = gradientsImage(aX - 1, aY);
                    B = gradientsImage(aX, aY);
                    C = gradientsImage(aX + 1, aY);

                    if (A > B)

                        if (A > C)
                            aX = aX - 1; % A
                        else
                            aX = aX + 1; % C
                        end

                    elseif (C > B)
                        aX = aX + 1; % C
                    end

                end

                if edgeImge(aX, aY) == 255 || gradientsImage(aX, aY) < gradientThreshold
                    return;
                end

                len = len + 1;
                pixels(len, :) = [aX, aY, 1];
            end

            len = len - 1;

            VerticalDownNavigator(aX, aY);
            VerticalUpNavigator(aX, aY);
        end

        function VerticalUpNavigator(aX, aY)

            if (edgeImge(aX, aY) ~= 255)
                duplicatePixelCount = duplicatePixelCount + 1;
            end

            len = len + 1;
            pixels(len, :) = [aX, aY, 2];

            while (directionsImage(aX, aY) == 1)

                edgeImge(aX, aY) = 255;

                if (edgeImge(aX, aY - 1) == 254)
                    edgeImge(aX, aY - 1) = 0;
                end

                if (edgeImge(aX, aY + 1) == 254)
                    edgeImge(aX, aY + 1) = 0;
                end

                %//   A B C
                %//     x

                % Follow next pixel in line
                aX = aX - 1; % B

                if (edgeImge(aX, aY) >= 254)% B
                elseif (edgeImge(aX, aY - 1) >= 254)% A
                    aY = aY - 1;
                elseif (edgeImge(aX, aY + 1) >= 254)% C
                    aY = aY + 1;
                else % A or C
                    A = gradientsImage(aX, aY - 1);
                    B = gradientsImage(aX, aY);
                    C = gradientsImage(aX, aY + 1);

                    if (A > B)

                        if (A > C)
                            aY = aY - 1; % A
                        else
                            aY = aY + 1; % C
                        end

                    elseif (C > B)
                        aY = aY + 1; % C
                    end

                end

                if edgeImge(aX, aY) == 255 || gradientsImage(aX, aY) < gradientThreshold
                    return;
                end

                len = len + 1;
                 pixels(len, :) = [aX, aY, 2];

            end

            len = len - 1;

            HorizontalRightNavigator(aX, aY);
            HorizontalLeftNavigator(aX, aY);

        end

        function VerticalDownNavigator(aX, aY)

            if (edgeImge(aX, aY) ~= 255)
                duplicatePixelCount = duplicatePixelCount + 1;
            end

            len = len + 1;
            pixels(len, :) = [aX, aY, 2];

            while (directionsImage(aX, aY) == 1)

                edgeImge(aX, aY) = 255;

                if (edgeImge(aX, aY + 1) == 254)
                    edgeImge(aX, aY + 1) = 0;
                end

                if (edgeImge(aX, aY - 1) == 254)
                    edgeImge(aX, aY - 1) = 0;
                end

                %//     x
                %//   A B C

                % Follow next pixel in line
                aX = aX + 1; % B

                if (edgeImge(aX, aY) >= 254)% B
                elseif (edgeImge(aX, aY + 1) >= 254)% C
                    aY = aY + 1;
                elseif (edgeImge(aX, aY - 1) >= 254)% A
                    aY = aY - 1;
                else % A or C
                    A = gradientsImage(aX, aY - 1);
                    B = gradientsImage(aX, aY);
                    C = gradientsImage(aX, aY + 1);

                    if (A > B)

                        if (A > C)
                            aY = aY - 1; % A
                        else
                            aY = aY + 1; % C
                        end

                    elseif (C > B)
                        aY = aY + 1; % C
                    end

                end

                if edgeImge(aX, aY) == 255 || gradientsImage(aX, aY) < gradientThreshold
                    return;
                end

                len = len + 1;
                pixels(len, :) = [aX, aY, 2];

            end

            len = len - 1;

            HorizontalRightNavigator(aX, aY);
            HorizontalLeftNavigator(aX, aY);

        end

    end

  
end