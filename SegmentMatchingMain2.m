clear all; close all; clc;
format shortG
addpath(genpath('./Dataset 6DoF'));
exp_folder = 'Impart';
K = dlmread(fullfile(exp_folder, 'mbtParams.txt'));
%imageTemplate = dlmread(fullfile(exp_folder, 'frameBuffer2.txt'))';
%dep_prev = dlmread(fullfile(exp_folder, 'depthBuffer2.txt'))';
imageTemplate = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\frameBuffer.txt"))' );
dep_prev = ( dlmread(fullfile("D:\Unity\mbt-integration-platform\depthBuffer.txt"))' );
% 
% DB = unique(dep_prev);
% maxD = DB(end);
% maxZ = DB(end-1);
% 
% dep_prev(dep_prev == maxD)= maxZ + 0.01;

IntrinsicParams = dlmread(fullfile("D:\Unity\mbt-integration-platform\k.txt"));
Pose = dlmread(fullfile("D:\Unity\mbt-integration-platform\pose.txt"));


cam = webcam(1);
videoPlayer = vision.VideoPlayer('Position', [500, -50, 1350, 750]);
cam.Resolution = '1280x720';
translationConstraint = 5;
segmentLenght = 15;
Height = 720;
Width = 1280;

N = 4;
statusWindow = zeros(5,1);
cnnCount = 1;
for k=1:1:25
    
    ROI = getROI(imageTemplate, translationConstraint);
    
    %Template ---------------------------------------------------
    
    [segmentsT, edgesT] = EDSegments(double(imageTemplate), ROI);
    segmentedOutline = zeros(Height,Width,3);
    segmentedMatch = zeros(Height,Width,3);
    
    imageTemplateRGB(:,:,1) =  imageTemplate*255;
    imageTemplateRGB(:,:,2) =  imageTemplate*255;
    imageTemplateRGB(:,:,3) =  imageTemplate*255;
    
    anchorPoints = findAnchors(segmentsT,N);
    
    %MBM ----------------------------------------------------------------------
    
    iCont = 0;
    matchStatus = 0;
    
    iKalman = 0;
    nKalman = 0;
    x0Kalman = 0;
    statusKalman = 0;
    
    while (matchStatus <= 5 && iCont <= 250)
        
        webCamRaw = snapshot(cam);
        webCamRaw = imresize(webCamRaw, [Height Width]);
        webCamGray = rgb2gray(webCamRaw);
        
        [segmentsQ, edgesQ] = EDSegments(double(webCamGray), ROI);
        
        sampling =  5;
        quality = 0;
        imgP = zeros(Height,Width);
        
        for k=1:sampling:size(segmentsT,1) - 1
            segmentedOutline(segmentsT(k,1)-1:segmentsT(k,1)+1 , segmentsT(k,2)-1:segmentsT(k,2)+1 , 2) = 1;
        end
        
        cc = 0;
        for iAncror=1:N:size(anchorPoints,1)-N
            
            x1 = anchorPoints(iAncror,2);
            y1 = anchorPoints(iAncror,1);
            x2 = anchorPoints(iAncror,4);
            y2 = anchorPoints(iAncror,3);
            
            P = findPerpendicularPoints(y1,x1,y2,x2,N);
            P = round(P);
            status = anchorValidation(P, edgesQ);
            
            for k=1:1:size(P,1)
                imgP(P(k,2),P(k,1)) = 1;
            end
            
            quality = quality + status;
            cc = cc +1;
            
        end
        
        sbmStatus = quality/cc;
        
        %Kalman filter ------------------------------------
        
        if sbmStatus >= .55
            
            zKalman = sbmStatus;
            if x0Kalman == 0
                x0Kalman = zKalman;
            end
            
            [x1Kalman, iKalman] = KalmanFilter(x0Kalman, zKalman, nKalman);
            x0Kalman = x1Kalman;
            nKalman = iKalman;
            
            if abs(x0Kalman/zKalman)>=0.55 && abs(x0Kalman/zKalman)<= 1.45
                statusKalman = 1;
            else
                statusKalman = 0;
            end
            
        end
        
        %-----------------------------------------------------
        
        if iKalman > 3 && statusKalman == 1
            
            imageMatch = ShowMatch(webCamRaw, imageTemplate, [0, 0]);
            step(videoPlayer, imageMatch);
            matchStatus = matchStatus + 1;
            statusWindow = zeros(5,1);
            statusKalman = 0;
            
            f1 = get6DoFTemplates(edgesT);
            f2 = get6DoFTemplates(edgesQ);

            figure(1)
            title("Original de EDLines");
            imshow(imageMatch)

            img_prev = f1;
            img_curr = f2.*ROI;
   
            % calculate the relative pose from two RGB-D frames
            num_levels = 5;
            debug6DoF(flip(imageTemplate), flip(webCamRaw), flip(img_curr), flip(img_prev), flip(dep_prev), K, num_levels, IntrinsicParams, Pose); 
            pause()
            clc
            
        else
            
            matchQ = ShowTemplate(segmentsQ,  webCamRaw);
            step(videoPlayer, matchQ + uint8(segmentedOutline*255));
            iCont = iCont + 1;
            
        end
        
    end
    
end
