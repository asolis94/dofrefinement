function [warped_image, valid_mask] = warpImage(img_curr, dep_prev, pose_rel, K, IntrinsicParams, Pose)
% warp the current image to the previous image coordinate
%
% INPUT:
%   img_curr: current grayscale image
%   dep_prev: previous depth image
%   pose_rel: relative pose between the current and previous frame
%   K: intrinsic camera parameters
%
% OUTPUT:
%   warped_image: warped image by the rigid transformation
%   valid_mask: valid pixels in the warped image

[height, width] = size(img_curr);

[pointclouds, valid_mask] = reprojectDepthImage(dep_prev, K, IntrinsicParams, Pose);
warped_pointclouds = warpPointCloud(pointclouds, pose_rel);

figure()
hold on;
pcshow(pointclouds, 'red');
pcshow(warped_pointclouds, 'green');
title('Red Original PC - Green Warped PC');
xlabel('X');
ylabel('Y');
zlabel('Z');

[warped_img_coords, warped_valid_mask] = projectPointCloud(warped_pointclouds, IntrinsicParams, K, height, width, Pose);

figure()
imshow(flip(dep_prev))
warpedImaaage = zeros(size(dep_prev));
for i = 1:length(warped_img_coords)
    warpedImaaage(uint32(warped_img_coords(i,2)), uint32(warped_img_coords(i,1))) = 255;
end
figure()
imshow(flip(warpedImaaage))

figure()
imshow(flip(dep_prev) + flip(warpedImaaage))

warped_intensities = interp2(img_curr, warped_img_coords(:, 1), warped_img_coords(:, 2), 'linear', 0);

valid_indices = find(valid_mask);
valid_indices = valid_indices(warped_valid_mask);
valid_mask = zeros(height, width);
valid_mask(valid_indices) = 1;
valid_mask = logical(valid_mask);

warped_image = zeros(height, width);
warped_image(valid_indices) = warped_intensities;

end