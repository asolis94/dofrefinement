function Zi = linearInterpolation(gI,warped_img_coordinates1,warped_img_coordinates2)

    Zi = zeros(size(warped_img_coordinates1));
    librarySize = size(gI);

    for kk = 1:1:size(warped_img_coordinates1,1)

        x = (warped_img_coordinates1(kk) - 1);
        y = (warped_img_coordinates2(kk) - 1);

        % Bilinear interpolation
        % Code is cloned from above for speed
        % Transform to unit square
        fxi = floor(x)+1;
        fyi = floor(y)+1;   % x_i and y_i
        dfxi = x-fxi+1;
        dfyi = y-fyi+1;     % Location in unit square

        % flagIn determines whether the requested location is inside of the
        % library arrays
        if fxi>2 && fxi<librarySize(2) && fyi>2 && fyi<librarySize(1)

            % Find bounding vertices
            ind1 = [fyi, + fxi];                 % Indices of (  x_i  ,  y_i  )
            ind2 = [fyi, + fxi+1];               % Indices of ( x_i+1 ,  y_i  )
            ind3 = [fyi+1, fxi+1];               % Indices of ( x_i+1 , y_i+1 )
            ind4 = [fyi+1, fxi];                 % Indices of (  x_i  , y_i+1 )

            Zi(kk,1) = gI(ind1(1),ind1(2))*(1-dfxi)*(1-dfyi);
%             Zi(kk,1) = gI(ind1(1),ind1(2))*(1-dfxi)*(1-dfyi) + ...
%                 gI(ind2(1),ind2(2))*dfxi*(1-dfyi) + ...
%                 gI(ind3(1),ind3(2))*dfxi*dfyi + ...
%                 gI(ind4(1),ind4(2))*(1-dfxi)*dfyi;

        end

    end
    