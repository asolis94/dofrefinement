function status = anchorValidation(P, edgesQ)

    c = 0; 
    status = 0; 
    for k=1:1:size(P,1)
        c = c + edgesQ(P(k,2),P(k,1)); 
    end
    
    if c >= 1
        status = 1; 
    end