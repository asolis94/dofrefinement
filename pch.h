#pragma once

#define _USE_MATH_DEFINES
#define CONST const
#define VOID void
#define IN
#define OUT

#include <limits>
#include <vector>
#include <list>
#include <stack>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <tuple>
#include <fstream>
#include <map>
#include <string>
#include <utility>
#include <sstream>
#include <chrono>
#include <tuple>

#include "Geometry.h"




