#include "mex.hpp"
#include "mexAdapter.hpp"
#include "pch.h"

using namespace matlab::data;
using matlab::mex::ArgumentList;
using namespace std;

class MexFunction : public matlab::mex::Function
{
public:
	template <typename T>
	TypedArray<T> TransformMatrixToMATLAB(
		CONST IN Matrix<T>& inputMatrix)
	{
		ArrayFactory factory;
		ArrayDimensions dimen = { (size_t)inputMatrix.resolution.width, (size_t)inputMatrix.resolution.height };
		TypedArray<T> outTypedArray = factory.createArray<T>(dimen);

		size_t i, j;
		for (i = 0; i < (size_t)inputMatrix.resolution.width; i++)
		{
			for (j = 0; j < (size_t)inputMatrix.resolution.height; j++)
			{
				outTypedArray[i][j] = inputMatrix.data[i][j];
			}
		}

		return outTypedArray;
	}

	template <typename T>
	Matrix<T> TransformMATLABToMatrix(
		CONST IN TypedArray<T>& inputMatrix,
		CONST IN Resolution& resolution)
	{
		Matrix<T> outputMatrix = Matrix<T>(resolution);

		size_t i, j;
		for (i = 0; i < (size_t)resolution.width; i++)
		{
			for (j = 0; j < (size_t)resolution.height; j++)
			{
				outputMatrix.data[i][j] = inputMatrix[i][j];
			}
		}

		return outputMatrix;
	}

	template <typename T>
	VOID TransformMatrixToVector(
		CONST IN Matrix<T>& inputMatrix,
		CONST IN Matrix<bool>& validMask,
		OUT std::vector<T>& outputVector)
	{
		outputVector.clear();

		int i, j;
		// Iterate i with inner j in C++, this was switched for MATLAB.
		for (j = 0; j < inputMatrix.resolution.height; ++j)
		{
			for (i = 0; i < inputMatrix.resolution.width; ++i)
			{
				if (validMask.data[i][j])
					outputVector.push_back(inputMatrix.data[i][j]);
			}
		}
	}

	VOID ConstructPyramidalReduction(
		CONST IN Matrix<double>& inputMatrix,
		CONST IN int pyrLevels,
		OUT std::vector<Matrix<double> >& pyramid)
	{
		pyramid.push_back(inputMatrix);

		Resolution resolution;
		int l, j, i;
		for (l = 1; l < pyrLevels; ++l)
		{
			resolution = pyramid[l - 1].resolution;

			pyramid.push_back(Matrix<double>(Resolution(round(resolution.width / 2.0f), round(resolution.height / 2.0f))));

			for (i = 0; i < pyramid[l].resolution.width; ++i)
			{
				for (j = 0; j < pyramid[l].resolution.height; ++j)
				{
					pyramid[l].data[i][j] = pyramid[l - 1].data[i * 2][j * 2];
				}
			}
		}
	}

	Matrix44d TwistExp(
		CONST IN std::vector<double>& increment)
	{
		double wx = increment[0];
		double wy = increment[1];
		double wz = increment[2];

		Matrix44d incre_pose;

		incre_pose.x[0][3] = increment[3];
		incre_pose.x[1][3] = increment[4];
		incre_pose.x[2][3] = increment[5];

		incre_pose.x[0][1] = -wz;
		incre_pose.x[0][2] = wy;
		incre_pose.x[1][0] = wz;
		incre_pose.x[1][2] = -wx;
		incre_pose.x[2][0] = -wy;
		incre_pose.x[2][1] = wx;

		return incre_pose;
	}

	Vec2d ConstructNormalizedCoord(
		CONST IN Vec2i& pixel,
		CONST IN Matrix44d& IntrinsicParamenters)
	{
		double fx = IntrinsicParamenters.x[0][0];
		double fy = IntrinsicParamenters.x[1][1];

		// Flip Cx and Cy in C++, it differs from MATLAB.
		double cy = IntrinsicParamenters.x[0][2];
		double cx = IntrinsicParamenters.x[1][2];

		double npx = (pixel.x - cx) / fx;
		double npy = (pixel.y - cy) / fy;

		return Vec2d(npx, npy);
	}

	VOID ReprojectDepthMatrix(
		CONST IN Matrix<double>& inputDepthMatrix,
		CONST IN Matrix44d& IntrinsicParamenters,
		OUT std::vector<Vec3d>& pointCloud)
	{
		pointCloud.clear();

		Vec2d npixel;

		size_t i, j;
		for (j = 0; j < (size_t)inputDepthMatrix.resolution.height; j++)
		{
			for (i = 0; i < (size_t)inputDepthMatrix.resolution.width; i++)
			{
				// Decrement i, j in C++, it is only incremented in order to match MATLAB.
				npixel = ConstructNormalizedCoord(Vec2i(i + 1, j + 1), IntrinsicParamenters);

				pointCloud.push_back(
					{
						npixel.x * inputDepthMatrix.data[i][j],
						npixel.y * inputDepthMatrix.data[i][j],
						inputDepthMatrix.data[i][j]
					}
				);
			}
		}
	}

	VOID WarpPointCloud(
		CONST IN Matrix44d pose,
		CONST IN std::vector<Vec3d>& pointCloud,
		OUT std::vector<Vec3d>& warpedPointCloud)
	{
		warpedPointCloud.clear();

		for (Vec3d point : pointCloud)
		{
			Vec3d warpedPoint;

			pose.GetRotation().multPointMatrixWarp(point, warpedPoint);
			Vec3d translation = pose.GetTranslation();
			warpedPoint = warpedPoint + Vec3d(translation.y, translation.x, translation.z);

			warpedPointCloud.push_back(warpedPoint);
		}
	}

	VOID ProjectPointCloud(
		CONST IN Matrix44d IntrinsicParamenters,
		CONST IN Resolution resolution,
		CONST IN std::vector<Vec3d>& warpedPointCloud,
		OUT std::vector<Vec2d>& warpedPixels,
		OUT std::vector<int>& validPoints)
	{
		warpedPixels.clear();
		validPoints.clear();

		Vec3d pixel;
		Vec3d imageCoordinates;

		int i;
		for (i = 0; i < warpedPointCloud.size(); ++i)
		{
			// Flip warpedPoint X and Y in C++, this is only valid in MATLAB.
			imageCoordinates.x = warpedPointCloud[i].y / warpedPointCloud[i].z;
			imageCoordinates.y = warpedPointCloud[i].x / warpedPointCloud[i].z;
			imageCoordinates.z = 1;

			IntrinsicParamenters.transposed().multDirMatrix(imageCoordinates, pixel);

			// Ask for Pixel X <= Widht and Y <= in C++, this is only valid in MATLAB.
			if (pixel.x > 0 && pixel.y > 0 && pixel.x <= resolution.height && pixel.y <= resolution.width)
			{
				warpedPixels.push_back({ pixel.x, pixel.y });

				validPoints.push_back(i);
			}
		}
	}

	template <typename T>
	VOID FilterValidPoints(
		CONST IN std::vector<int>& validPointsIndex,
		OUT std::vector<T>& v)
	{
		std::vector<T> temporal;

		for (int index : validPointsIndex)
		{
			temporal.push_back(v[index]);
		}

		v = temporal;
	}

	VOID SpatialGradient(
		CONST IN Matrix<double>& InputMatrix,
		OUT Matrix<double>& Gx,
		OUT Matrix<double>& Gy)
	{
		Gx = Matrix<double>(InputMatrix.resolution);
		Gy = Matrix<double>(InputMatrix.resolution);

		int x, y;
		int	width = InputMatrix.resolution.width - 1;
		int	height = InputMatrix.resolution.height - 1;
		for (x = 1; x < width; ++x)
		{
			for (y = 1; y < height; ++y)
			{
				Gx.data[x][y] = (InputMatrix.data[x][y + 1] - InputMatrix.data[x][y - 1]) / 2.0f;
				Gy.data[x][y] = (InputMatrix.data[x + 1][y] - InputMatrix.data[x - 1][y]) / 2.0f;
			}
		}
	}

	VOID LinearInterpolation(
		CONST IN Matrix<double>& InputMatrix,
		CONST IN std::vector<Vec2d>& warpedPixels,
		OUT std::vector<double>& InterpoledVector)
	{
		InterpoledVector.clear();
		InterpoledVector.resize(warpedPixels.size());

		Vec2d warpedPixel, fi, dfi, P1, P2, P3, P4;

		int i;
		for (i = 0; i < warpedPixels.size(); ++i)
		{
			// Flip warpedPixel X and Y in C++, this is only valid in MATLAB and decrement values by 1.
			warpedPixel = Vec2d(warpedPixels[i].y - 2, warpedPixels[i].x - 2);

			fi.x = floor(warpedPixel.x) + 1;
			fi.y = floor(warpedPixel.y) + 1;

			dfi.x = warpedPixel.x - fi.x + 1;
			dfi.y = warpedPixel.y - fi.y + 1;

			if (fi.x >= 1 && fi.y >= 1 && fi.x < InputMatrix.resolution.width - 1 && fi.y < InputMatrix.resolution.height - 1)
			{
				P1 = Vec2d(fi.x, fi.y);
				P2 = Vec2d(fi.x + 1, fi.y);
				P3 = Vec2d(fi.x + 1, fi.y + 1);
				P4 = Vec2d(fi.x, fi.y + 1);

				InterpoledVector[i] =
					InputMatrix[P1.x][P1.y] * (1 - dfi.x) * (1 - dfi.y) +
					InputMatrix[P2.x][P2.y] * dfi.x * (1 - dfi.y) +
					InputMatrix[P3.x][P3.y] * dfi.x * dfi.y +
					InputMatrix[P4.x][P4.y] * (1 - dfi.x) * dfi.y;
			}
		}
	}

	template <typename T>
	Matrix<T> KroneckerProduct(
		Matrix<T>& A,
		Matrix<T>& B)
	{
		Matrix<T> C = Matrix<T>(
			Resolution(
				A.resolution.width * B.resolution.width,
				A.resolution.height * B.resolution.height
			));

		int i, j, k, l;
		for (i = 0; i < A.resolution.width; ++i)
		{
			for (j = 0; j < A.resolution.height; ++j)
			{
				for (k = 0; k < B.resolution.width; ++k)
				{
					for (l = 0; l < B.resolution.height; ++l)
					{
						C.data[i * B.resolution.width + k][j * B.resolution.height + l] = A.data[i][j] * B.data[k][l];
					}
				}
			}
		}

		return C;
	}

	VOID CalculateWarpingJacobian(
		CONST IN std::vector<Vec3d>& warpedPointCloud,
		CONST IN std::vector<Vec3d>& pointCloud,
		CONST IN Matrix44d& IntrinsicParameters,
		CONST IN Matrix44d& pose,
		OUT Matrix<double>& Jacobian)
	{
		double fx = IntrinsicParameters.x[0][0];
		double fy = IntrinsicParameters.x[1][1];

		// Step - 1: Projection gradient
		std::vector < std::vector<Vec2d>> projectionGradient;
		projectionGradient.resize(warpedPointCloud.size(), std::vector<Vec2d>(3));

		double projGradientZ1, projGradientZ2;

		int i, j, k;
		for (i = 0; i < warpedPointCloud.size(); ++i)
		{
			projectionGradient[i][0] = Vec2d(fx / warpedPointCloud[i].z, 0.0f);
			projectionGradient[i][1] = Vec2d(0.0f, fy / warpedPointCloud[i].z);

			projGradientZ1 = -fx * warpedPointCloud[i].x / pow(warpedPointCloud[i].z, 2);
			projGradientZ2 = -fy * warpedPointCloud[i].y / pow(warpedPointCloud[i].z, 2);

			if (isinf(projGradientZ1))
				projGradientZ1 = 0.0f;

			if (isinf(projGradientZ2))
				projGradientZ2 = 0.0f;

			projectionGradient[i][2] = Vec2d(
				projGradientZ1,
				projGradientZ2
			);
		}

		// Step - 2: rigid point - transformation gradient
		Matrix<double> A = Matrix<double>(Resolution(3, 3));
		A.data[0][0] = A.data[1][1] = A.data[2][2] = 1.0f;

		Matrix<double> B = Matrix<double>(Resolution(pointCloud.size(), 3));

		for (i = 0; i < pointCloud.size(); ++i)
		{
			// Flip pointCloud X and Y in C++, this is only valid in MATLAB.
			B.data[i][0] = pointCloud[i].y;
			B.data[i][1] = pointCloud[i].x;
			B.data[i][2] = pointCloud[i].z;
		}

		Matrix<double> rotationGradient = KroneckerProduct(A, B);
		Matrix<double> translationGradient = KroneckerProduct(A, Matrix<double>(Resolution(pointCloud.size(), 1), 1.0f));

		Vec3i dim(rotationGradient.resolution.width / 3, 3, rotationGradient.resolution.height + translationGradient.resolution.height);

		std::vector<std::vector<std::vector<double>>> rigidTransformationGradient;
		rigidTransformationGradient.resize(dim.x, std::vector<std::vector<double>>(dim.y, std::vector<double>(dim.z)));

		for (i = 0; i < dim.x; ++i)
		{
			for (k = 0; k < dim.z; ++k)
			{
				if (k < rotationGradient.resolution.height)
				{
					rigidTransformationGradient[i][0][k] = rotationGradient.data[i + 0 * dim.x][k];
					rigidTransformationGradient[i][1][k] = rotationGradient.data[i + 1 * dim.x][k];
					rigidTransformationGradient[i][2][k] = rotationGradient.data[i + 2 * dim.x][k];
				}
				else
				{
					rigidTransformationGradient[i][0][k] = translationGradient.data[i + 0 * dim.x][k - rotationGradient.resolution.height];
					rigidTransformationGradient[i][1][k] = translationGradient.data[i + 1 * dim.x][k - rotationGradient.resolution.height];
					rigidTransformationGradient[i][2][k] = translationGradient.data[i + 2 * dim.x][k - rotationGradient.resolution.height];
				}
			}
		}

		// Step - 3: twist motion gradient
		Matrix<double> twistGradient = Matrix<double>(Resolution(12, 6));

		twistGradient.data[0][1] = pose.x[0][2];
		twistGradient.data[1][1] = pose.x[1][2];
		twistGradient.data[2][1] = pose.x[2][2];

		twistGradient.data[0][2] = -pose.x[0][1];
		twistGradient.data[1][2] = -pose.x[1][1];
		twistGradient.data[2][2] = -pose.x[2][1];

		twistGradient.data[3][0] = -pose.x[0][2];
		twistGradient.data[4][0] = -pose.x[1][2];
		twistGradient.data[5][0] = -pose.x[2][2];

		twistGradient.data[3][2] = pose.x[0][0];
		twistGradient.data[4][2] = pose.x[1][0];
		twistGradient.data[5][2] = pose.x[2][0];

		twistGradient.data[6][0] = pose.x[0][1];
		twistGradient.data[7][0] = pose.x[1][1];
		twistGradient.data[8][0] = pose.x[2][1];

		twistGradient.data[6][1] = -pose.x[0][0];
		twistGradient.data[7][1] = -pose.x[1][0];
		twistGradient.data[8][1] = -pose.x[2][0];

		twistGradient.data[10][0] = -pose.x[2][3];
		twistGradient.data[11][0] = pose.x[1][3];

		twistGradient.data[9][1] = pose.x[2][3];
		twistGradient.data[11][1] = -pose.x[0][3];

		twistGradient.data[9][2] = -pose.x[1][3];
		twistGradient.data[10][2] = pose.x[0][3];

		twistGradient.data[9][3] = 1.0f;
		twistGradient.data[10][4] = 1.0f;
		twistGradient.data[11][5] = 1.0f;

		// Step - 4: Calculate the chained gradient
		Matrix<double> chainedGradient = Matrix<double>(projectionGradient.size() * 2, 12);

		for (k = 0; k < 12; ++k)
		{
			Matrix<double> projectionGradientX = Matrix<double>(projectionGradient.size(), 3);
			Matrix<double> projectionGradientY = Matrix<double>(projectionGradient.size(), 3);

			for (i = 0; i < projectionGradient.size(); ++i)
			{
				projectionGradientX[i][0] = projectionGradient[i][0].x;
				projectionGradientX[i][1] = projectionGradient[i][1].x;
				projectionGradientX[i][2] = projectionGradient[i][2].y; // In C++ use X instad of Y

				projectionGradientY[i][0] = projectionGradient[i][0].y;
				projectionGradientY[i][1] = projectionGradient[i][1].y;
				projectionGradientY[i][2] = projectionGradient[i][2].x;  // In C++ use Y instad of X
			}

			Matrix<double> rigidGradient = Matrix<double>(rigidTransformationGradient.size(), 3);

			for (i = 0; i < rigidTransformationGradient.size(); ++i)
			{
				for (j = 0; j < 3; ++j)
				{
					rigidGradient[i][j] = rigidTransformationGradient[i][j][k];
				}
			}

			std::vector<double> dotX = projectionGradientX.dotProduct(rigidGradient);
			std::vector<double> dotY = projectionGradientY.dotProduct(rigidGradient);

			for (i = 0; i < dotX.size(); ++i)
			{
				chainedGradient[i][k] = dotX[i];
				chainedGradient[i + dotY.size()][k] = dotY[i];
			}
		}

		Jacobian = chainedGradient * twistGradient;
	}

	VOID CalculateCompositiveJacobian(
		CONST IN Matrix<double>& Jacobian,
		CONST IN std::vector<double>& Gx_WebCamInterpoled,
		CONST IN std::vector<double>& Gy_WebCamInterpoled,
		OUT Matrix<double>& CompositiveJacobian)
	{
		CompositiveJacobian = Matrix<double>(Jacobian.rows / 2, 6);
		int i, j;
		for (i = 0; i < CompositiveJacobian.rows; ++i)
		{
			for (j = 0; j < CompositiveJacobian.columns; ++j)
			{
				CompositiveJacobian[i][j] =
					Gx_WebCamInterpoled[i] * Jacobian[i][j] +
					Gy_WebCamInterpoled[i] * Jacobian[i + CompositiveJacobian.rows][j];
			}
		}
	}

	void operator()(ArgumentList outputs, ArgumentList inputs)
	{
		TypedArray<double> inputWebCam = std::move(inputs[0]);
		TypedArray<double> inputTemplate = std::move(inputs[1]);
		TypedArray<double> inputDepthTemplate = std::move(inputs[2]);
		TypedArray<double> K = std::move(inputs[3]);
		int pyramidLevels = inputs[4][0];
		size_t height = inputs[5][0];
		size_t width = inputs[6][0];

		Resolution resolution = Resolution(height, width);

		Matrix<double> imageWebCam = TransformMATLABToMatrix(inputWebCam, resolution);
		Matrix<double> imageTemplate = TransformMATLABToMatrix(inputTemplate, resolution);
		Matrix<double> imageDepthTemplate = TransformMATLABToMatrix(inputDepthTemplate, resolution);

		// Pyramid Reduction
		std::vector<Matrix<double> > pyramidWebCam, pyramidTemplate, pyramidDepthBuffer;
		ConstructPyramidalReduction(imageWebCam, pyramidLevels, pyramidWebCam);
		ConstructPyramidalReduction(imageTemplate, pyramidLevels, pyramidTemplate);
		ConstructPyramidalReduction(imageDepthTemplate, pyramidLevels, pyramidDepthBuffer);

		// Initialize the relative pose and its increment
		Matrix44d PoseRelative;
		std::vector<double> Motion(6);

		// Convert the vectorize motion increment to update the relative pose
		Matrix44d IncrementMatrix = TwistExp(Motion);
		PoseRelative = IncrementMatrix * PoseRelative;

		// Modify the camera parameters to fit each pyramid, we start with the smallest pyramid
		Matrix44d K_pyr;
		K_pyr.x[0][0] = K[0][0] / pow(pyramidLevels - 1, 2);
		K_pyr.x[1][1] = K[1][1] / pow(pyramidLevels - 1, 2);
		K_pyr.x[0][2] = K[0][2] / pow(pyramidLevels - 1, 2);
		K_pyr.x[1][2] = K[1][2] / pow(pyramidLevels - 1, 2);

		// Iterate over every pyramid level, start with the highest level
		std::vector<Vec3d> pointCloud;
		std::vector<Vec3d> pointCloudIterative;

		std::vector<Vec3d> warpedPointCloud;
		std::vector<Vec3d> warpedPointCloudIterative;

		std::vector<Vec2d> warpedPixels; std::vector<int> validPoints;
		std::vector<Vec2d> warpedPixelsIterative;

		Matrix<double> Gx_WebCam, Gy_WebCam;
		std::vector<double> Gx_WebCamInterpoled, Gy_WebCamInterpoled;
		std::vector<double> Gt_Template;
		std::vector<double> Gt_WebCam;
		Matrix<double> GradientTemporal;
		Matrix<double> Jacobian;
		Matrix<double> CompositiveJacobian;
		Matrix<double> numerador;
		Matrix<double> denominador;
		Matrix<double> Increment;

		//int N = pyramidLevels - 1;
		for (int N = pyramidLevels - 1; N >= 0; --N)
		{
			// Get valid point clouds in the previous frame
			ReprojectDepthMatrix(pyramidDepthBuffer[N], K_pyr, pointCloud);
			// temp var
			pointCloudIterative = pointCloud;

			// Warp pointclouds
			WarpPointCloud(PoseRelative, pointCloud, warpedPointCloud);
			// temp var
			warpedPointCloudIterative = warpedPointCloud;

			// Project PointCloud and prune invalid points
			ProjectPointCloud(K_pyr, pyramidDepthBuffer[N].resolution, warpedPointCloud, warpedPixels, validPoints);
			// temp var
			warpedPixelsIterative = warpedPixels;

			FilterValidPoints(validPoints, pointCloud);
			FilterValidPoints(validPoints, warpedPointCloud);

			// Obtain the Spatial Gradient for the WebCam pyramid image.
			SpatialGradient(pyramidWebCam[N], Gx_WebCam, Gy_WebCam);

			LinearInterpolation(Gx_WebCam, warpedPixels, Gx_WebCamInterpoled);
			LinearInterpolation(Gy_WebCam, warpedPixels, Gy_WebCamInterpoled);

			// Temporal visual difference for the Template pyramid image and the WebCam pyramid image.
			TransformMatrixToVector(pyramidTemplate[N], Matrix<bool>(pyramidTemplate[N].resolution, true), Gt_Template);
			FilterValidPoints(validPoints, Gt_Template);

			LinearInterpolation(pyramidWebCam[N], warpedPixels, Gt_WebCam);

			GradientTemporal = Matrix<double>(Gt_WebCam.size(), 1);
			for (int i = 0; i < GradientTemporal.rows; ++i)
			{
				GradientTemporal[i][0] = Gt_WebCam[i] - Gt_Template[i];
			}

			// Calculate the Warping Jacobian
			CalculateWarpingJacobian(warpedPointCloud, pointCloud, K_pyr, PoseRelative, Jacobian);

			// Calculate the Compositive Jacobian
			CalculateCompositiveJacobian(Jacobian, Gx_WebCamInterpoled, Gy_WebCamInterpoled, CompositiveJacobian);

			// Calculate the Increment motion
			Increment =
				(-(CompositiveJacobian.transpose() * CompositiveJacobian)).inverseMatrix() * (CompositiveJacobian.transpose() * GradientTemporal);

			numerador = -(CompositiveJacobian.transpose() * CompositiveJacobian);//.inverseMatrix();
			denominador = (CompositiveJacobian.transpose() * GradientTemporal);

			TransformMatrixToVector(Increment, Matrix<bool>(Increment.resolution, true), Motion);

			// Get the current relative pose
			IncrementMatrix = TwistExp(Motion);
			PoseRelative = IncrementMatrix * PoseRelative;

			// Increase the focal lenght in every iteration.
			K_pyr.x[0][0] = K_pyr.x[0][0] * 2;
			K_pyr.x[1][1] = K_pyr.x[1][1] * 2;
			K_pyr.x[0][2] = K_pyr.x[0][2] * 2;
			K_pyr.x[1][2] = K_pyr.x[1][2] * 2;
		}

		cout << PoseRelative << endl;
		cout << "\n";

		// ********************
		//		RETURNS
		// ********************

		ArrayFactory factory;
		ArrayDimensions dimen = { pointCloud.size(), 3 };
		TypedArray<double> outPointCloud = factory.createArray<double>(dimen);
		TypedArray<double> outWarpedPointCloud = factory.createArray<double>(dimen);

		size_t i, j, k;
		for (i = 0; i < pointCloud.size(); i++)
		{
			outPointCloud[i][0] = pointCloud[i].y;
			outPointCloud[i][1] = pointCloud[i].x;
			outPointCloud[i][2] = pointCloud[i].z;

			outWarpedPointCloud[i][0] = warpedPointCloud[i].y;
			outWarpedPointCloud[i][1] = warpedPointCloud[i].x;
			outWarpedPointCloud[i][2] = warpedPointCloud[i].z;
		}

		dimen = { warpedPixels.size(), 2 };
		TypedArray<double> outWarpedPixels = factory.createArray<double>(dimen);
		for (i = 0; i < warpedPixels.size(); i++)
		{
			outWarpedPixels[i][0] = warpedPixels[i].x;
			outWarpedPixels[i][1] = warpedPixels[i].y;
		}

		dimen = { Gx_WebCamInterpoled.size(), 1 };
		TypedArray<double> outGx_WebCamInterpoled = factory.createArray<double>(dimen);
		for (i = 0; i < Gx_WebCamInterpoled.size(); i++)
		{
			outGx_WebCamInterpoled[i][0] = Gx_WebCamInterpoled[i];
		}
		TypedArray<double> outGy_WebCamInterpoled = factory.createArray<double>(dimen);
		for (i = 0; i < Gy_WebCamInterpoled.size(); i++)
		{
			outGy_WebCamInterpoled[i][0] = Gy_WebCamInterpoled[i];
		}

		ArrayDimensions dimen2 = { pointCloudIterative.size(), 3 };
		TypedArray<double> outPointCloudItarative = factory.createArray<double>(dimen2);
		TypedArray<double> outWarpedPointCloudIterative = factory.createArray<double>(dimen2);
		for (i = 0; i < pointCloudIterative.size(); i++)
		{
			outPointCloudItarative[i][0] = pointCloudIterative[i].y;
			outPointCloudItarative[i][1] = pointCloudIterative[i].x;
			outPointCloudItarative[i][2] = pointCloudIterative[i].z;

			outWarpedPointCloudIterative[i][0] = warpedPointCloudIterative[i].y;
			outWarpedPointCloudIterative[i][1] = warpedPointCloudIterative[i].x;
			outWarpedPointCloudIterative[i][2] = warpedPointCloudIterative[i].z;
		}

		dimen = { warpedPixelsIterative.size(), 2 };
		TypedArray<double> outWarpedPixelsIterative = factory.createArray<double>(dimen);
		for (i = 0; i < warpedPixelsIterative.size(); i++)
		{
			outWarpedPixelsIterative[i][0] = warpedPixelsIterative[i].x;
			outWarpedPixelsIterative[i][1] = warpedPixelsIterative[i].y;
		}

		outputs[0] = outPointCloud;
		outputs[1] = outWarpedPointCloud;
		outputs[2] = outWarpedPixels;

		outputs[3] = TransformMatrixToMATLAB(Gx_WebCam);
		outputs[4] = TransformMatrixToMATLAB(Gy_WebCam);
		outputs[5] = outGx_WebCamInterpoled;
		outputs[6] = outGy_WebCamInterpoled;
		outputs[7] = TransformMatrixToMATLAB(GradientTemporal);
		outputs[8] = TransformMatrixToMATLAB(CompositiveJacobian);
		outputs[9] = TransformMatrixToMATLAB(numerador);
		outputs[10] = TransformMatrixToMATLAB(numerador.inverseMatrix());
		//outputs[11] = TransformMatrixToMATLAB(denominador);
		outputs[11] = TransformMatrixToMATLAB(pyramidDepthBuffer[3]);
	}
};