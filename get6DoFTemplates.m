function gradients = get6DoFTemplates(rawImage)

    gaussianKernel = fspecial('gaussian', 5, 2);

    %RGB to Gray 
    grayImage = rawImage;

    %Gaussian smoothing 
    gaussianImage = conv2(grayImage, gaussianKernel, 'same');

    %Gradient operations
    gradientsImage = gradientOperations(gaussianImage);    
    gradients = gradientsImage; 
    
    %Gradient operations -----------------------------------------------
    %--------------------------------------------------------------------------

    function gradientsImage = gradientOperations(gaussianImage)

        gradientThreshold = 36;
        gxImage = zeros(size(gaussianImage));
        gyImage = zeros(size(gaussianImage));
        gradientsImage = zeros(size(gaussianImage));

        for x = 2:1:size(gaussianImage, 1)-1
            for y = 2:1:size(gaussianImage, 2)-1

                gxImage(x,y) = abs (-gaussianImage(x-1,y-1) + gaussianImage(x-1,y+1) + ...
                                    -2*gaussianImage(x,y-1) + 2*gaussianImage(x,y+1) + ...
                                    -gaussianImage(x+1,y-1) + gaussianImage(x+1,y+1));

                gyImage(x,y) = abs (-gaussianImage(x-1,y-1) + gaussianImage(x+1,y-1) + ...
                                    -2*gaussianImage(x-1,y) + 2*gaussianImage(x+1,y) + ...
                                    -gaussianImage(x-1, y+1) + gaussianImage(x+1, y+1));

                gradientsImage(x,y) = gxImage(x,y) + gyImage(x,y);

                if gradientsImage(x,y) >= gradientThreshold
                    gradientsImage(x,y) =  1;
                else
                    gradientsImage(x,y) =  0; 
                end

            end
        end

    end    

  
end