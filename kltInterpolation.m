% function fI = kltInterpolation(iX,iY,gI,oX,oY)
% if size(iX,2)==size(oX,1)
%     fI = gI;
% else
%     fI = zeros(size(oX,1),size(oX,2));
%     for i=1:1:size(oX,1)
%         for j=1:1:size(oX,2)
%             fx1 = ((iY(i+1)-oY(i,j))/(iY(i+1)-iY(i)))*gI(i,j) + ((oY(i,j)-iY(i))/(iY(i+1)-iY(i)))*gI(i+1,j);
%             fx2 = ((iY(i+1)-oY(i,j))/(iY(i+1)-iY(i)))*gI(i,j+1) + ((oY(i,j)-iY(i))/(iY(i+1)-iY(i)))*gI(i+1,j+1);
%             fxy = ((iX(j+1)-oX(i,j))/(iX(j+1)-iX(j)))*fx1 + ((oX(i,j)-iX(j))/(iX(j+1)-iX(j)))*fx2;
%             fI(i,j) = fxy;
%         end
%     end
% end

function fI = kltInterpolation(imageInput, indexX, indexY)
if size(iX,2)==size(oX,1)
    fI = gI;
else
    fI = zeros(size(oX,1),size(oX,2));
    for i=1:1:size(oX,1)
        for j=1:1:size(oX,2)
            fx1 = ((iY(i+1)-oY(i,j))/(iY(i+1)-iY(i)))*gI(i,j) + ((oY(i,j)-iY(i))/(iY(i+1)-iY(i)))*gI(i+1,j);
            fx2 = ((iY(i+1)-oY(i,j))/(iY(i+1)-iY(i)))*gI(i,j+1) + ((oY(i,j)-iY(i))/(iY(i+1)-iY(i)))*gI(i+1,j+1);
            fxy = ((iX(j+1)-oX(i,j))/(iX(j+1)-iX(j)))*fx1 + ((oX(i,j)-iX(j))/(iX(j+1)-iX(j)))*fx2;
            fI(i,j) = fxy;
        end
    end
end