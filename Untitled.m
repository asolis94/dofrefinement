%obj = readObj('impart.obj');

[height, width] = size(img_curr);

[frameBuffer1] = ...
    raster(obj.v, obj.vn, obj.f.v, length(obj.v),length(obj.f.v), height, width, IntrinsicParams, Pose);


[frameBuffer2] = ...
    raster(obj.v, obj.vn, obj.f.v, length(obj.v),length(obj.f.v), height, width, IntrinsicParams,pose_rel *  Pose );

% figure()
% imshow(flip(uint8(frameBuffer2')));

figure()
imageMatch6DoF =( ShowMatch(imageMatch, (uint8(frameBuffer2')), [0, 0]) );
imshow(flip(imageMatch6DoF))

% figure()
% frameBufferDif1 = zeros(height, width, 3);
% frameBufferDif1(:,:,1) = flip(uint8(frameBuffer1'));
% frameBufferDif2 = zeros(height, width, 3);
% frameBufferDif2(:,:,2) = flip(uint8(frameBuffer2'));
% 
% imshow(frameBufferDif1 + frameBufferDif2);