#include "mex.hpp"
#include "mexAdapter.hpp"
#include "pch.h"

using namespace matlab::data;
using matlab::mex::ArgumentList;
using namespace std;

class MexFunction : public matlab::mex::Function
{
public:
	template <typename T>
	TypedArray<T> TransformMatrixToMATLAB(
		CONST IN Matrix<T>& inputMatrix)
	{
		ArrayFactory factory;
		ArrayDimensions dimen = { (size_t)inputMatrix.resolution.width, (size_t)inputMatrix.resolution.height };
		TypedArray<T> outTypedArray = factory.createArray<T>(dimen);

		size_t i, j;
		for (i = 0; i < (size_t)inputMatrix.resolution.width; i++)
		{
			for (j = 0; j < (size_t)inputMatrix.resolution.height; j++)
			{
				outTypedArray[i][j] = inputMatrix.data[i][j];
			}
		}

		return outTypedArray;
	}

	template <typename T>
	Matrix<T> TransformMATLABToMatrix(
		CONST IN TypedArray<T>& inputMatrix,
		CONST IN Resolution& resolution)
	{
		Matrix<T> outputMatrix = Matrix<T>(resolution);

		size_t i, j;
		for (i = 0; i < (size_t)resolution.width; i++)
		{
			for (j = 0; j < (size_t)resolution.height; j++)
			{
				outputMatrix.data[i][j] = inputMatrix[i][j];
			}
		}

		return outputMatrix;
	}

	VOID ConstructPyramidalReduction(
		CONST IN Matrix<double>& inputMatrix,
		CONST IN int pyrLevels,
		OUT std::vector<Matrix<double> >& pyramid)
	{
		pyramid.push_back(inputMatrix);

		Resolution resolution;
		int l, j, i;
		for (l = 1; l < pyrLevels; ++l)
		{
			resolution = pyramid[l - 1].resolution;
			pyramid.push_back(Matrix<double>(Resolution(resolution.width / 2, resolution.height / 2)));

			for (i = 0; i < pyramid[l].resolution.width; ++i)
			{
				for (j = 0; j < pyramid[l].resolution.height; ++j)
				{
					pyramid[l].data[i][j] = pyramid[l - 1].data[i * 2][j * 2];
				}
			}
		}
	}

	Matrix44f TwistExp(
		CONST IN std::vector<float>& increment)
	{
		float wx = increment[0];
		float wy = increment[1];
		float wz = increment[2];

		Matrix44f incre_pose;

		incre_pose.x[0][3] = increment[3];
		incre_pose.x[1][3] = increment[4];
		incre_pose.x[2][3] = increment[5];

		incre_pose.x[0][1] = -wz;
		incre_pose.x[0][2] = wy;
		incre_pose.x[1][0] = wz;
		incre_pose.x[1][2] = -wx;
		incre_pose.x[2][0] = -wy;
		incre_pose.x[2][1] = wx;

		return incre_pose;
	}

	Vec2f ConstructNormalizedCoord(
		CONST IN Vec2i& pixel,
		CONST IN Matrix44f& IntrinsicParamenters)
	{
		float fx = IntrinsicParamenters.x[0][0];
		float fy = IntrinsicParamenters.x[1][1];

		// Flipped CY with CX because MATLAB it is different
		float cy = IntrinsicParamenters.x[0][2];
		float cx = IntrinsicParamenters.x[1][2];

		float npx = (pixel.x - cx) / fx;
		float npy = (pixel.y - cy) / fy;

		return Vec2f(npx, npy);
	}

	std::vector<Vec3f> ReprojectDepthMatrix(
		CONST IN Matrix<double>& inputDepthMatrix,
		CONST IN Matrix44f& IntrinsicParamenters)
	{
		std::vector<Vec3f> PointCloud;
		Vec2f npixel;

		size_t i, j;
		for (j = 0; j < (size_t)inputDepthMatrix.resolution.height; j++)
		{
			for (i = 0; i < (size_t)inputDepthMatrix.resolution.width; i++)
			{
				// Decrement i, j in C++, it is only incremented in order to match MATLAB.
				npixel = ConstructNormalizedCoord(Vec2i(i + 1, j + 1), IntrinsicParamenters);

				PointCloud.push_back(
					{
						npixel.x * (float)inputDepthMatrix.data[i][j],
						npixel.y * (float)inputDepthMatrix.data[i][j],
						(float)inputDepthMatrix.data[i][j]
					}
				);
			}
		}

		return PointCloud;
	}

	std::vector<Vec3f> WarpPointCloud(
		CONST IN Matrix44f pose,
		OUT std::vector<Vec3f>& pointCloud)
	{
		std::vector<Vec3f> warpedPointCloud;

		for (Vec3f& point : pointCloud)
		{
			Vec3f warpedPoint;
			pose.multPointMatrix(point, warpedPoint);
			warpedPointCloud.push_back(warpedPoint);
		}

		return warpedPointCloud;
	}

	std::vector<Vec2f> ProjectPointCloud(
		CONST IN Matrix44f IntrinsicParamenters,
		CONST IN Resolution resolution,
		CONST IN std::vector<Vec3f>& warpedPointCloud,
		OUT std::vector<int>& validPoints)
	{
		Vec3f pixel;
		Vec3f imageCoordinates;
		std::vector<Vec2f> pixels;

		int i;
		for (i = 0; i < warpedPointCloud.size(); ++i)
		{
			// Flip warpedPoint X and Y in C++, this is only valid in MATLAB.
			imageCoordinates.x = warpedPointCloud[i].y / warpedPointCloud[i].z;
			imageCoordinates.y = warpedPointCloud[i].x / warpedPointCloud[i].z;
			imageCoordinates.z = 1;

			IntrinsicParamenters.transposed().multDirMatrix(imageCoordinates, pixel);

			// Ask for Pixel X <= Widht and Y <= in C++, this is only valid in MATLAB.
			if (pixel.x > 0 && pixel.y > 0 && pixel.x <= resolution.height && pixel.y <= resolution.width)
			{
				pixels.push_back({ pixel.x, pixel.y });

				validPoints.push_back(i);
			}
		}

		return pixels;
	}

	VOID SpatialGradient(
		CONST IN Matrix<double>& InputMatrix,
		OUT Matrix<double>& Gx,
		OUT Matrix<double>& Gy)
	{
		Gx = Matrix<double>(InputMatrix.resolution);
		Gy = Matrix<double>(InputMatrix.resolution);

		int x, y;
		int	width = InputMatrix.resolution.width - 1;
		int	height = InputMatrix.resolution.height - 1;
		for (x = 1; x < width; ++x)
		{
			for (y = 1; y < height; ++y)
			{
				Gx.data[x][y] = (InputMatrix.data[x][y + 1] - InputMatrix.data[x][y - 1]) / 2.0f;
				Gy.data[x][y] = (InputMatrix.data[x + 1][y] - InputMatrix.data[x - 1][y]) / 2.0f;
			}
		}
	}

	std::vector<double> LinearInterpolation(
		CONST IN Matrix<double>& InputMatrix,
		CONST IN std::vector<Vec2f>& warpedPixels)
	{
		std::vector<double> interpoledPoints;
		interpoledPoints.resize(warpedPixels.size());

		Vec2f warpedPixel, fi, dfi, P1, P2, P3, P4;

		int i;
		for (i = 0; i < warpedPixels.size(); ++i)
		{
			// Flip warpedPixel X and Y in C++, this is only valid in MATLAB and decrement values by 1.
			warpedPixel = Vec2f(warpedPixels[i].y - 2, warpedPixels[i].x - 2);

			fi.x = floor(warpedPixel.x) + 1;
			fi.y = floor(warpedPixel.y) + 1;

			dfi.x = warpedPixel.x - fi.x + 1;
			dfi.y = warpedPixel.y - fi.y + 1;

			if (fi.x >= 1 && fi.y >= 1 && fi.x < InputMatrix.resolution.width - 1 && fi.y < InputMatrix.resolution.height - 1)
			{
				P1 = Vec2f(fi.x, fi.y);
				P2 = Vec2f(fi.x + 1, fi.y);
				P3 = Vec2f(fi.x + 1, fi.y + 1);
				P4 = Vec2f(fi.x, fi.y + 1);

				interpoledPoints[i] =
					InputMatrix[P1.x][P1.y] * (1 - dfi.x) * (1 - dfi.y) +
					InputMatrix[P2.x][P2.y] * dfi.x * (1 - dfi.y) +
					InputMatrix[P3.x][P3.y] * dfi.x * dfi.y +
					InputMatrix[P4.x][P4.y] * (1 - dfi.x) * dfi.y;
			}
		}

		return interpoledPoints;
	}

	Matrix<float> CalculateWarpingJacobian(
		CONST IN std::vector<Vec3f>& warpedPointCloud,
		CONST IN std::vector<Vec3f>& pointCloud,
		CONST IN Matrix44f& IntrinsicParameters,
		CONST IN Matrix44f& pose)
	{
		float fx = IntrinsicParameters.x[0][0];
		float fy = IntrinsicParameters.x[1][1];

		// Step - 1: Projection gradient
		std::vector < std::vector<Vec2f>> projectionGradient;
		projectionGradient.resize(warpedPointCloud.size(), std::vector<Vec2f>(3));

		float projGradientZ1, projGradientZ2;

		int i, j, k;
		for (i = 0; i < warpedPointCloud.size(); ++i)
		{
			projectionGradient[i][0] = Vec2f(fx / warpedPointCloud[i].z, 0.0f);
			projectionGradient[i][1] = Vec2f(0.0f, fy / warpedPointCloud[i].z);

			projGradientZ1 = -fx * warpedPointCloud[i].x / pow(warpedPointCloud[i].z, 2);
			projGradientZ2 = -fy * warpedPointCloud[i].y / pow(warpedPointCloud[i].z, 2);

			if (isinf(projGradientZ1))
				projGradientZ1 = 0.0f;

			if (isinf(projGradientZ2))
				projGradientZ2 = 0.0f;

			projectionGradient[i][2] = Vec2f(
				projGradientZ1,
				projGradientZ2
			);
		}

		// Step - 2: rigid point - transformation gradient
		Matrix<float> A = Matrix<float>(Resolution(3, 3));
		A.data[0][0] = A.data[1][1] = A.data[2][2] = 1.0f;

		Matrix<float> B = Matrix<float>(Resolution(pointCloud.size(), 3));

		for (i = 0; i < pointCloud.size(); ++i)
		{
			// Flip pointCloud X and Y in C++, this is only valid in MATLAB.
			B.data[i][0] = pointCloud[i].y;
			B.data[i][1] = pointCloud[i].x;
			B.data[i][2] = pointCloud[i].z;
		}

		Matrix<float> rotationGradient = KroneckerProduct(A, B);
		Matrix<float> translationGradient = KroneckerProduct(A, Matrix<float>(Resolution(pointCloud.size(), 1), 1.0f));

		std::vector<std::vector<std::vector<float>>> rigidTransformationGradient;
		Vec3i dim(rotationGradient.resolution.width / 3, 3, rotationGradient.resolution.height + translationGradient.resolution.height);

		rigidTransformationGradient.resize(
			dim.x, std::vector<std::vector<float>>(dim.y, std::vector<float>(dim.z))
		);

		for (i = 0; i < dim.x; ++i)
		{
			for (k = 0; k < dim.z; ++k)
			{
				if (k < rotationGradient.resolution.height)
				{
					rigidTransformationGradient[i][0][k] = rotationGradient.data[i + 0 * dim.x][k];
					rigidTransformationGradient[i][1][k] = rotationGradient.data[i + 1 * dim.x][k];
					rigidTransformationGradient[i][2][k] = rotationGradient.data[i + 2 * dim.x][k];
				}
				else
				{
					rigidTransformationGradient[i][0][k] = translationGradient.data[i + 0 * dim.x][k - rotationGradient.resolution.height];
					rigidTransformationGradient[i][1][k] = translationGradient.data[i + 1 * dim.x][k - rotationGradient.resolution.height];
					rigidTransformationGradient[i][2][k] = translationGradient.data[i + 2 * dim.x][k - rotationGradient.resolution.height];
				}
			}
		}

		// Step - 3: twist motion gradient
		Matrix<float> twistGradient = Matrix<float>(Resolution(12, 6));

		twistGradient.data[0][1] = pose.x[0][2];
		twistGradient.data[1][1] = pose.x[1][2];
		twistGradient.data[2][1] = pose.x[2][2];

		twistGradient.data[0][2] = -pose.x[0][1];
		twistGradient.data[1][2] = -pose.x[1][1];
		twistGradient.data[2][2] = -pose.x[2][1];

		twistGradient.data[3][0] = -pose.x[0][2];
		twistGradient.data[4][0] = -pose.x[1][2];
		twistGradient.data[5][0] = -pose.x[2][2];

		twistGradient.data[3][2] = pose.x[0][0];
		twistGradient.data[4][2] = pose.x[1][0];
		twistGradient.data[5][2] = pose.x[2][0];

		twistGradient.data[6][0] = pose.x[0][1];
		twistGradient.data[7][0] = pose.x[1][1];
		twistGradient.data[8][0] = pose.x[2][1];

		twistGradient.data[6][1] = -pose.x[0][0];
		twistGradient.data[7][1] = -pose.x[1][0];
		twistGradient.data[8][1] = -pose.x[2][0];

		twistGradient.data[10][0] = -pose.x[2][3];
		twistGradient.data[11][0] = pose.x[1][3];

		twistGradient.data[9][1] = pose.x[2][3];
		twistGradient.data[11][1] = -pose.x[0][3];

		twistGradient.data[9][2] = -pose.x[1][3];
		twistGradient.data[10][2] = pose.x[0][3];

		twistGradient.data[9][3] = 1.0f;
		twistGradient.data[10][4] = 1.0f;
		twistGradient.data[11][5] = 1.0f;

		// Step - 4: Calculate the chained gradient
		Matrix<float> chainedGradient = Matrix<float>(projectionGradient.size() * 2, 12);

		for (k = 0; k < 12; ++k)
		{
			Matrix<float> projectionGradientX = Matrix<float>(projectionGradient.size(), 3);
			Matrix<float> projectionGradientY = Matrix<float>(projectionGradient.size(), 3);

			for (i = 0; i < projectionGradient.size(); ++i)
			{
				projectionGradientX[i][0] = projectionGradient[i][0].x;
				projectionGradientX[i][1] = projectionGradient[i][1].x;
				projectionGradientX[i][2] = projectionGradient[i][2].y; // In C++ use X instad of Y

				projectionGradientY[i][0] = projectionGradient[i][0].y;
				projectionGradientY[i][1] = projectionGradient[i][1].y;
				projectionGradientY[i][2] = projectionGradient[i][2].x;  // In C++ use Y instad of X
			}

			Matrix<float> rigidGradient = Matrix<float>(rigidTransformationGradient.size(), 3);

			for (i = 0; i < rigidTransformationGradient.size(); ++i)
			{
				for (j = 0; j < 3; ++j)
				{
					rigidGradient[i][j] = rigidTransformationGradient[i][j][k];
				}
			}

			std::vector<float> dotX = projectionGradientX.dotProduct(rigidGradient);
			std::vector<float> dotY = projectionGradientY.dotProduct(rigidGradient);

			for (i = 0; i < dotX.size(); ++i)
			{
				chainedGradient[i][k] = dotX[i];
				chainedGradient[i + dotY.size()][k] = dotY[i];
			}
		}

		Matrix<float> Jacobian = chainedGradient * twistGradient;

		return Jacobian;
	}

	template <typename T>
	Matrix<T> KroneckerProduct(
		Matrix<T>& A,
		Matrix<T>& B)
	{
		Matrix<T> C = Matrix<T>(
			Resolution(
				A.resolution.width * B.resolution.width,
				A.resolution.height * B.resolution.height
			));

		int i, j, k, l;
		for (i = 0; i < A.resolution.width; ++i)
		{
			for (j = 0; j < A.resolution.height; ++j)
			{
				for (k = 0; k < B.resolution.width; ++k)
				{
					for (l = 0; l < B.resolution.height; ++l)
					{
						C.data[i * B.resolution.width + k][j * B.resolution.height + l] = A.data[i][j] * B.data[k][l];
					}
				}
			}
		}

		return C;
	}

	template <typename T>
	VOID FilterValidPoints(
		CONST IN std::vector<int>& validPointsIndex,
		OUT std::vector<T>& v)
	{
		std::vector<T> temporal;

		for (int index : validPointsIndex)
		{
			temporal.push_back(v[index]);
		}

		v = temporal;
	}

	template <typename T>
	std::vector<T> TransformMatrixToVector(
		CONST IN Matrix<T> inputMatrix,
		CONST IN Matrix<bool> validMask)
	{
		std::vector<T> v;

		int i, j;
		// Iterate i with inner j in C++, this was switched for MATLAB.
		for (j = 0; j < inputMatrix.resolution.height; ++j)
		{
			for (i = 0; i < inputMatrix.resolution.width; ++i)
			{
				if (validMask.data[i][j])
					v.push_back(inputMatrix.data[i][j]);
			}
		}

		return v;
	}

	void operator()(ArgumentList outputs, ArgumentList inputs)
	{
		TypedArray<double> inputWebCam = std::move(inputs[0]);
		TypedArray<double> inputTemplate = std::move(inputs[1]);
		TypedArray<double> inputDepthTemplate = std::move(inputs[2]);
		TypedArray<double> K = std::move(inputs[3]);
		int pyramidLevels = inputs[4][0];
		size_t height = inputs[5][0];
		size_t width = inputs[6][0];

		Resolution resolution = Resolution(height, width);

		Matrix<double> imageWebCam = TransformMATLABToMatrix(inputWebCam, resolution);
		Matrix<double> imageTemplate = TransformMATLABToMatrix(inputTemplate, resolution);
		Matrix<double> imageDepthTemplate = TransformMATLABToMatrix(inputDepthTemplate, resolution);

		// Pyramid Reduction
		std::vector<Matrix<double> > pyramidWebCam, pyramidTemplate, pyramidDepthBuffer;
		ConstructPyramidalReduction(imageWebCam, pyramidLevels, pyramidWebCam);
		ConstructPyramidalReduction(imageTemplate, pyramidLevels, pyramidTemplate);
		ConstructPyramidalReduction(imageDepthTemplate, pyramidLevels, pyramidDepthBuffer);

		// Initialize the relative poseand its increment
		Matrix44f pose_rel;
		std::vector<float> motion(6);

		// Convert the vectorize motion increment to update the relative pose
		Matrix44f increment = TwistExp(motion);
		pose_rel = increment * pose_rel;

		// Modify the camera parameters to fit each pyramid, we start with the smallest pyramid
		Matrix44f K_pyr;
		K_pyr.x[0][0] = K[0][0] / pow(pyramidLevels - 1, 2);
		K_pyr.x[1][1] = K[1][1] / pow(pyramidLevels - 1, 2);
		K_pyr.x[0][2] = K[0][2] / pow(pyramidLevels - 1, 2);
		K_pyr.x[1][2] = K[1][2] / pow(pyramidLevels - 1, 2);

		// Iterate over every pyramid level, start with the highest level
		std::vector<Vec3f> pointCloud;
		std::vector<Vec3f> warpedPointCloud;
		std::vector<Vec2f> warpedPixels;
		std::vector<double> Gx_WebCamInterpoled;
		std::vector<double> Gy_WebCamInterpoled;
		std::vector<double> Gt_Template;
		std::vector<double> Gt_WebCam;
		Matrix<float> GradientTemporal;
		Matrix<double> Gx_WebCam, Gy_WebCam;
		Matrix<float> Jacobian;
		Matrix<float> CompositiveJacobian;
		Matrix<float> Increment;
		Matrix44f IncrementMatrix;

		int N;
		for (N = pyramidLevels - 1; N >= 0; --N)
		{
			// Get valid point clouds in the previous
			pointCloud = ReprojectDepthMatrix(pyramidDepthBuffer[N], K_pyr);

			// Warp pointclouds
			warpedPointCloud = WarpPointCloud(pose_rel, pointCloud);

			// Project PointCloud and prune invalid points
			std::vector<int> validPoints;
			warpedPixels = ProjectPointCloud(K_pyr, pyramidDepthBuffer[N].resolution, warpedPointCloud, validPoints);

			FilterValidPoints(validPoints, pointCloud);
			FilterValidPoints(validPoints, warpedPointCloud);

			// Obtain the Spatial Gradient for the WebCam pyramid image.
			SpatialGradient(pyramidWebCam[N], Gx_WebCam, Gy_WebCam);

			Gx_WebCamInterpoled = LinearInterpolation(Gx_WebCam, warpedPixels);
			Gy_WebCamInterpoled = LinearInterpolation(Gy_WebCam, warpedPixels);

			break;

			// Temporal visual difference for the Template pyramid image and the WebCam pyramid image.
			Gt_Template = TransformMatrixToVector(pyramidTemplate[N], Matrix<bool>(pyramidTemplate[N].resolution, true));
			FilterValidPoints(validPoints, Gt_Template);

			Gt_WebCam = BilinearInterpolation(pyramidWebCam[N], warpedPixels);

			GradientTemporal = Matrix<float>(Gt_WebCam.size(), 1);
			for (int i = 0; i < GradientTemporal.rows; ++i)
			{
				GradientTemporal[i][0] = Gt_WebCam[i] - Gt_Template[i];
			}

			// Calculate the Warping Jacobian
			Jacobian = CalculateWarpingJacobian(warpedPointCloud, pointCloud, K_pyr, pose_rel,
				projectionGradient, rigidTransformationGradient, twistGradient, chainedGradient);

			// Calculate the Compositive Jacobian
			CompositiveJacobian = Matrix<float>(GradientTemporal.rows, 6);
			for (int i = 0; i < CompositiveJacobian.rows; ++i)
			{
				for (int j = 0; j < CompositiveJacobian.columns; ++j)
				{
					CompositiveJacobian[i][j] =
						Gx_WebCamInterpoled[i] * Jacobian[i][j] +
						Gy_WebCamInterpoled[i] * Jacobian[i + CompositiveJacobian.rows][j];
				}
			}

			// Calculate the Increment motion
			Increment = (-(CompositiveJacobian.transpose() * CompositiveJacobian)).inverseMatrix() * (CompositiveJacobian.transpose() * GradientTemporal);

			IncrementMatrix = TwistExp(TransformMatrixToVector(Increment, Matrix<bool>(Increment.resolution, true)));

			pose_rel = IncrementMatrix * pose_rel;

			cout << pose_rel << endl;

			break;
			// Increase the focal lenght in every iteration.
			K_pyr.x[0][0] = pow(K_pyr.x[0][0], 2);
			K_pyr.x[1][1] = pow(K_pyr.x[1][1], 2);
			K_pyr.x[0][2] = pow(K_pyr.x[0][2], 2);
			K_pyr.x[1][2] = pow(K_pyr.x[1][2], 2);
		}

		ArrayFactory factory;
		ArrayDimensions dimen = { pointCloud.size(), 3 };
		TypedArray<double> outPointCloud = factory.createArray<double>(dimen);
		TypedArray<double> outWarpedPointCloud = factory.createArray<double>(dimen);

		size_t i, j, k;
		for (i = 0; i < pointCloud.size(); i++)
		{
			outPointCloud[i][0] = pointCloud[i].y;
			outPointCloud[i][1] = pointCloud[i].x;
			outPointCloud[i][2] = pointCloud[i].z;

			outWarpedPointCloud[i][0] = warpedPointCloud[i].y;
			outWarpedPointCloud[i][1] = warpedPointCloud[i].x;
			outWarpedPointCloud[i][2] = warpedPointCloud[i].z;
		}

		dimen = { warpedPixels.size(), 2 };
		TypedArray<double> outWarpedPixels = factory.createArray<double>(dimen);
		for (i = 0; i < warpedPixels.size(); i++)
		{
			outWarpedPixels[i][0] = warpedPixels[i].x;
			outWarpedPixels[i][1] = warpedPixels[i].y;
		}

		dimen = { Gx_WebCamInterpoled.size(), 1 };
		TypedArray<double> outGx_WebCamInterpoled = factory.createArray<double>(dimen);
		for (i = 0; i < Gx_WebCamInterpoled.size(); i++)
		{
			outGx_WebCamInterpoled[i][0] = Gx_WebCamInterpoled[i];
		}
		TypedArray<double> outGy_WebCamInterpoled = factory.createArray<double>(dimen);
		for (i = 0; i < Gy_WebCamInterpoled.size(); i++)
		{
			outGy_WebCamInterpoled[i][0] = Gy_WebCamInterpoled[i];
		}

		dimen = { projectionGradient.size(), 2, 3 };
		TypedArray<double> outprojGradient = factory.createArray<double>(dimen);
		for (i = 0; i < projectionGradient.size(); i++)
		{
			outprojGradient[i][0][0] = projectionGradient[i][0].x;
			outprojGradient[i][1][0] = projectionGradient[i][0].y;
			outprojGradient[i][0][1] = projectionGradient[i][1].x;
			outprojGradient[i][1][1] = projectionGradient[i][1].y;
			outprojGradient[i][1][2] = projectionGradient[i][2].x;
			outprojGradient[i][0][2] = projectionGradient[i][2].y;
		}

		dimen = { rigidTransformationGradient.size(), 3, 12 };
		TypedArray<double>  outRigidTransformationGradient = factory.createArray<double>(dimen);
		for (k = 0; k < 12; ++k)
		{
			for (i = 0; i < rigidTransformationGradient.size(); ++i)
			{
				for (j = 0; j < 3; ++j)
				{
					outRigidTransformationGradient[i][j][k] = rigidTransformationGradient[i][j][k];
				}
			}
		}

		outputs[0] = outPointCloud;
		outputs[1] = outWarpedPointCloud;
		outputs[2] = outWarpedPixels;
		outputs[3] = outGx_WebCamInterpoled;
		outputs[4] = outGy_WebCamInterpoled;
		outputs[5] = TransformMatrixToMATLAB(GradientTemporal);
		outputs[6] = outprojGradient;
		outputs[7] = outRigidTransformationGradient;
		outputs[8] = TransformMatrixToMATLAB(twistGradient);
		outputs[9] = TransformMatrixToMATLAB(chainedGradient);
		outputs[10] = TransformMatrixToMATLAB(Jacobian);
		outputs[11] = TransformMatrixToMATLAB(CompositiveJacobian);
		outputs[12] = TransformMatrixToMATLAB(Increment);
		outputs[13] = TransformMatrixToMATLAB(Increment);
		outputs[14] = TransformMatrixToMATLAB(Gx_WebCam);
		outputs[15] = TransformMatrixToMATLAB(Gy_WebCam);
	}
};