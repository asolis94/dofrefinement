function [vec_out] = multPointMatrix(m, vec_in)

vec_out = zeros(1,3);

vec_out(1) = vec_in(1) * m(1,1) + vec_in(2) * m(1,2) + vec_in(3) * m(1,3) + m(1,4);
vec_out(2) = vec_in(1) * m(2,1) + vec_in(2) * m(2,2) + vec_in(3) * m(2,3) + m(2,4);
vec_out(3) = vec_in(1) * m(3,1) + vec_in(2) * m(3,2) + vec_in(3) * m(3,3) + m(3,4);
w          = vec_in(1) * m(4,1) + vec_in(2) * m(4,2) + vec_in(3) * m(4,3) + m(4,4);

if (w ~= 1)
    vec_out(1) =  vec_out(1) / w;
    vec_out(2) =  vec_out(2) / w;
    vec_out(3) =  vec_out(3) / w;
end

end